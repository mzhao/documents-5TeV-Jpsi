{
//=========Macro generated from canvas: ptCanvas/pt Canvas
//=========  (Fri Aug 28 20:01:46 2015) by ROOT version5.34/25
   TCanvas *ptCanvas = new TCanvas("ptCanvas", "pt Canvas",8,31,500,400);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   ptCanvas->Range(-2.592593,-0.07315918,15.92593,0.4494064);
   ptCanvas->SetFillColor(0);
   ptCanvas->SetBorderMode(0);
   ptCanvas->SetBorderSize(2);
   ptCanvas->SetTickx(1);
   ptCanvas->SetTicky(1);
   ptCanvas->SetLeftMargin(0.14);
   ptCanvas->SetRightMargin(0.05);
   ptCanvas->SetTopMargin(0.07);
   ptCanvas->SetBottomMargin(0.14);
   ptCanvas->SetFrameLineWidth(2);
   ptCanvas->SetFrameBorderMode(0);
   ptCanvas->SetFrameLineWidth(2);
   ptCanvas->SetFrameBorderMode(0);
   
   TH1D *hpt3 = new TH1D("hpt3","forward MC p_{T} [GeV/c]",10,0,15);
   hpt3->SetBinContent(1,0.1952979);
   hpt3->SetBinContent(2,0.2801349);
   hpt3->SetBinContent(3,0.2145001);
   hpt3->SetBinContent(4,0.1397551);
   hpt3->SetBinContent(5,0.07920818);
   hpt3->SetBinContent(6,0.04287008);
   hpt3->SetBinContent(7,0.02398299);
   hpt3->SetBinContent(8,0.01398286);
   hpt3->SetBinContent(9,0.00836451);
   hpt3->SetBinContent(10,0.001903412);
   hpt3->SetBinError(1,0.0007160547);
   hpt3->SetBinError(2,0.0008575926);
   hpt3->SetBinError(3,0.0007504315);
   hpt3->SetBinError(4,0.0006057328);
   hpt3->SetBinError(5,0.0004560184);
   hpt3->SetBinError(6,0.0003354861);
   hpt3->SetBinError(7,0.0002509279);
   hpt3->SetBinError(8,0.0001915999);
   hpt3->SetBinError(9,0.0001481896);
   hpt3->SetBinError(10,7.069094e-05);
   hpt3->SetMinimum(0);
   hpt3->SetMaximum(0.4128268);
   hpt3->SetEntries(380895);
   hpt3->SetStats(0);
   hpt3->SetLineColor(4);
   hpt3->SetLineWidth(2);
   hpt3->SetMarkerColor(4);
   hpt3->SetMarkerStyle(22);
   hpt3->GetXaxis()->SetTitle("p_{T} [GeV / c]");
   hpt3->GetXaxis()->SetNdivisions(505);
   hpt3->GetXaxis()->SetLabelFont(42);
   hpt3->GetXaxis()->SetLabelOffset(0.015);
   hpt3->GetXaxis()->SetLabelSize(0.05);
   hpt3->GetXaxis()->SetTitleSize(0.06);
   hpt3->GetXaxis()->SetTitleFont(42);
   hpt3->GetYaxis()->SetTitle("Arbitrary scale");
   hpt3->GetYaxis()->SetLabelFont(42);
   hpt3->GetYaxis()->SetLabelSize(0.05);
   hpt3->GetYaxis()->SetTitleSize(0.06);
   hpt3->GetYaxis()->SetTitleOffset(0.84);
   hpt3->GetYaxis()->SetTitleFont(42);
   hpt3->GetZaxis()->SetLabelFont(42);
   hpt3->GetZaxis()->SetLabelSize(0.05);
   hpt3->GetZaxis()->SetTitleSize(0.06);
   hpt3->GetZaxis()->SetTitleFont(42);
   hpt3->Draw("");
   
   TH1D *hpt1 = new TH1D("hpt1","forward p_{T} [GeV/c]",10,0,15);
   hpt1->SetBinContent(1,0.1777235);
   hpt1->SetBinContent(2,0.3259439);
   hpt1->SetBinContent(3,0.1873168);
   hpt1->SetBinContent(4,0.1473395);
   hpt1->SetBinContent(5,0.07654559);
   hpt1->SetBinContent(6,0.05284167);
   hpt1->SetBinContent(7,0.0256202);
   hpt1->SetBinContent(8,-0.0005805526);
   hpt1->SetBinContent(9,0.004265886);
   hpt1->SetBinContent(10,0.002983572);
   hpt1->SetBinError(1,0.04895656);
   hpt1->SetBinError(2,0.05293549);
   hpt1->SetBinError(3,0.03806699);
   hpt1->SetBinError(4,0.02613208);
   hpt1->SetBinError(5,0.01783929);
   hpt1->SetBinError(6,0.014355);
   hpt1->SetBinError(7,0.009651573);
   hpt1->SetBinError(8,0.002224819);
   hpt1->SetBinError(9,0.004640952);
   hpt1->SetBinError(10,0.003526896);
   hpt1->SetMinimum(0);
   hpt1->SetMaximum(0.4128268);
   hpt1->SetEntries(4045);
   hpt1->SetStats(0);
   hpt1->SetLineWidth(2);
   hpt1->SetMarkerStyle(20);
   hpt1->GetXaxis()->SetNdivisions(505);
   hpt1->GetXaxis()->SetLabelFont(42);
   hpt1->GetXaxis()->SetLabelOffset(0.015);
   hpt1->GetXaxis()->SetLabelSize(0.05);
   hpt1->GetXaxis()->SetTitleSize(0.06);
   hpt1->GetXaxis()->SetTitleFont(42);
   hpt1->GetYaxis()->SetTitle("Arbitrary scale");
   hpt1->GetYaxis()->SetLabelFont(42);
   hpt1->GetYaxis()->SetLabelSize(0.05);
   hpt1->GetYaxis()->SetTitleSize(0.06);
   hpt1->GetYaxis()->SetTitleOffset(0.84);
   hpt1->GetYaxis()->SetTitleFont(42);
   hpt1->GetZaxis()->SetLabelFont(42);
   hpt1->GetZaxis()->SetLabelSize(0.05);
   hpt1->GetZaxis()->SetTitleSize(0.06);
   hpt1->GetZaxis()->SetTitleFont(42);
   hpt1->Draw("same");
   
   TH1D *hpt2 = new TH1D("hpt2","backward p_{T} [GeV/c]",10,0,15);
   hpt2->SetBinContent(1,0.2272317);
   hpt2->SetBinContent(2,0.3749074);
   hpt2->SetBinContent(3,0.1062606);
   hpt2->SetBinContent(4,0.1599856);
   hpt2->SetBinContent(5,0.06379594);
   hpt2->SetBinContent(6,0.04804553);
   hpt2->SetBinContent(7,0.01321586);
   hpt2->SetBinContent(8,0.010844);
   hpt2->SetBinContent(9,-0.004286705);
   hpt2->SetBinError(1,0.1295827);
   hpt2->SetBinError(2,0.1364106);
   hpt2->SetBinError(3,0.08752072);
   hpt2->SetBinError(4,0.05775114);
   hpt2->SetBinError(5,0.03406398);
   hpt2->SetBinError(6,0.02666165);
   hpt2->SetBinError(7,0.01587601);
   hpt2->SetBinError(8,0.0130183);
   hpt2->SetBinError(9,0.003031158);
   hpt2->SetMinimum(0);
   hpt2->SetMaximum(0.4128268);
   hpt2->SetEntries(1975);
   hpt2->SetStats(0);
   hpt2->SetLineColor(2);
   hpt2->SetLineWidth(2);
   hpt2->SetMarkerColor(2);
   hpt2->SetMarkerStyle(21);
   hpt2->GetXaxis()->SetNdivisions(505);
   hpt2->GetXaxis()->SetLabelFont(42);
   hpt2->GetXaxis()->SetLabelOffset(0.015);
   hpt2->GetXaxis()->SetLabelSize(0.05);
   hpt2->GetXaxis()->SetTitleSize(0.06);
   hpt2->GetXaxis()->SetTitleFont(42);
   hpt2->GetYaxis()->SetTitle("Arbitrary scale");
   hpt2->GetYaxis()->SetLabelFont(42);
   hpt2->GetYaxis()->SetLabelSize(0.05);
   hpt2->GetYaxis()->SetTitleSize(0.06);
   hpt2->GetYaxis()->SetTitleOffset(0.84);
   hpt2->GetYaxis()->SetTitleFont(42);
   hpt2->GetZaxis()->SetLabelFont(42);
   hpt2->GetZaxis()->SetLabelSize(0.05);
   hpt2->GetZaxis()->SetTitleSize(0.06);
   hpt2->GetZaxis()->SetTitleFont(42);
   hpt2->Draw("same");
   
   TH1D *hpt3 = new TH1D("hpt3","forward MC p_{T} [GeV/c]",10,0,15);
   hpt3->SetBinContent(1,0.1952979);
   hpt3->SetBinContent(2,0.2801349);
   hpt3->SetBinContent(3,0.2145001);
   hpt3->SetBinContent(4,0.1397551);
   hpt3->SetBinContent(5,0.07920818);
   hpt3->SetBinContent(6,0.04287008);
   hpt3->SetBinContent(7,0.02398299);
   hpt3->SetBinContent(8,0.01398286);
   hpt3->SetBinContent(9,0.00836451);
   hpt3->SetBinContent(10,0.001903412);
   hpt3->SetBinError(1,0.0007160547);
   hpt3->SetBinError(2,0.0008575926);
   hpt3->SetBinError(3,0.0007504315);
   hpt3->SetBinError(4,0.0006057328);
   hpt3->SetBinError(5,0.0004560184);
   hpt3->SetBinError(6,0.0003354861);
   hpt3->SetBinError(7,0.0002509279);
   hpt3->SetBinError(8,0.0001915999);
   hpt3->SetBinError(9,0.0001481896);
   hpt3->SetBinError(10,7.069094e-05);
   hpt3->SetMinimum(0);
   hpt3->SetMaximum(0.4128268);
   hpt3->SetEntries(380895);
   hpt3->SetStats(0);
   hpt3->SetLineColor(4);
   hpt3->SetLineWidth(2);
   hpt3->SetMarkerColor(4);
   hpt3->SetMarkerStyle(22);
   hpt3->GetXaxis()->SetTitle("p_{T} [GeV / c]");
   hpt3->GetXaxis()->SetNdivisions(505);
   hpt3->GetXaxis()->SetLabelFont(42);
   hpt3->GetXaxis()->SetLabelOffset(0.015);
   hpt3->GetXaxis()->SetLabelSize(0.05);
   hpt3->GetXaxis()->SetTitleSize(0.06);
   hpt3->GetXaxis()->SetTitleFont(42);
   hpt3->GetYaxis()->SetTitle("Arbitrary scale");
   hpt3->GetYaxis()->SetLabelFont(42);
   hpt3->GetYaxis()->SetLabelSize(0.05);
   hpt3->GetYaxis()->SetTitleSize(0.06);
   hpt3->GetYaxis()->SetTitleOffset(0.84);
   hpt3->GetYaxis()->SetTitleFont(42);
   hpt3->GetZaxis()->SetLabelFont(42);
   hpt3->GetZaxis()->SetLabelSize(0.05);
   hpt3->GetZaxis()->SetTitleSize(0.06);
   hpt3->GetZaxis()->SetTitleFont(42);
   hpt3->Draw("same");
   
   TLegend *leg = new TLegend(0.58,0.55,0.79,0.69,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(2);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("hpt1","pPb(Fwd)","lpe");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hpt2","pPb(Bwd)","lpe");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hpt3","pp MC","lpe");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(22);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   TLatex *   tex = new TLatex(0.58,0.78,"#splitline{#splitline{LHCb}{Preliminary}}{#scale[0.8]{pPb #sqrt{s_{NN}} = 5 TeV}}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
   ptCanvas->Modified();
   ptCanvas->cd();
   ptCanvas->SetSelected(ptCanvas);
}
