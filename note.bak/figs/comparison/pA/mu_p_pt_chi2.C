{
//=========Macro generated from canvas: pptCanvas/ppt Canvas
//=========  (Tue Sep 15 00:48:29 2015) by ROOT version5.34/18
   TCanvas *pptCanvas = new TCanvas("pptCanvas", "ppt Canvas",55,52,500,400);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   pptCanvas->Range(-2.592593,-0.1117907,15.92593,0.6867143);
   pptCanvas->SetFillColor(0);
   pptCanvas->SetBorderMode(0);
   pptCanvas->SetBorderSize(2);
   pptCanvas->SetTickx(1);
   pptCanvas->SetTicky(1);
   pptCanvas->SetLeftMargin(0.14);
   pptCanvas->SetRightMargin(0.05);
   pptCanvas->SetTopMargin(0.07);
   pptCanvas->SetBottomMargin(0.14);
   pptCanvas->SetFrameLineWidth(2);
   pptCanvas->SetFrameBorderMode(0);
   pptCanvas->SetFrameLineWidth(2);
   pptCanvas->SetFrameBorderMode(0);
   
   TH1D *hppt3 = new TH1D("hppt3","forward MC p_{T} [GeV/c]",10,0,15);
   hppt3->SetBinContent(1,0.1922204);
   hppt3->SetBinContent(2,0.5310731);
   hppt3->SetBinContent(3,0.1822434);
   hppt3->SetBinContent(4,0.05818102);
   hppt3->SetBinContent(5,0.02192083);
   hppt3->SetBinContent(6,0.009051019);
   hppt3->SetBinContent(7,0.003641344);
   hppt3->SetBinContent(8,0.001413936);
   hppt3->SetBinContent(9,0.000254944);
   hppt3->SetBinError(1,0.0007498297);
   hppt3->SetBinError(2,0.001253414);
   hppt3->SetBinError(3,0.0007400045);
   hppt3->SetBinError(4,0.0004221168);
   hppt3->SetBinError(5,0.0002596176);
   hppt3->SetBinError(6,0.0001688383);
   hppt3->SetBinError(7,0.0001072529);
   hppt3->SetBinError(8,6.702573e-05);
   hppt3->SetBinError(9,2.78662e-05);
   hppt3->SetMinimum(0);
   hppt3->SetMaximum(0.6308189);
   hppt3->SetEntries(380895);
   hppt3->SetStats(0);
   hppt3->SetLineColor(4);
   hppt3->SetLineWidth(2);
   hppt3->SetMarkerColor(4);
   hppt3->SetMarkerStyle(22);
   hppt3->GetXaxis()->SetTitle("p_{T}(#mu^{+})");
   hppt3->GetXaxis()->SetNdivisions(505);
   hppt3->GetXaxis()->SetLabelFont(42);
   hppt3->GetXaxis()->SetLabelOffset(0.015);
   hppt3->GetXaxis()->SetLabelSize(0.05);
   hppt3->GetXaxis()->SetTitleSize(0.06);
   hppt3->GetXaxis()->SetTitleFont(42);
   hppt3->GetYaxis()->SetTitle("Arbitrary scale");
   hppt3->GetYaxis()->SetLabelFont(42);
   hppt3->GetYaxis()->SetLabelSize(0.05);
   hppt3->GetYaxis()->SetTitleSize(0.06);
   hppt3->GetYaxis()->SetTitleOffset(0.84);
   hppt3->GetYaxis()->SetTitleFont(42);
   hppt3->GetZaxis()->SetLabelFont(42);
   hppt3->GetZaxis()->SetLabelSize(0.05);
   hppt3->GetZaxis()->SetTitleSize(0.06);
   hppt3->GetZaxis()->SetTitleFont(42);
   hppt3->Draw("");
   
   TH1D *hppt1 = new TH1D("hppt1","forward p_{T} [GeV/c]",10,0,15);
   hppt1->SetBinContent(1,0.1903052);
   hppt1->SetBinContent(2,0.5732283);
   hppt1->SetBinContent(3,0.1499224);
   hppt1->SetBinContent(4,0.04984026);
   hppt1->SetBinContent(5,0.0233136);
   hppt1->SetBinContent(6,0.01042383);
   hppt1->SetBinContent(7,0.003490304);
   hppt1->SetBinContent(8,-0.0005238472);
   hppt1->SetBinError(1,0.0461145);
   hppt1->SetBinError(2,0.06807916);
   hppt1->SetBinError(3,0.02932214);
   hppt1->SetBinError(4,0.01486732);
   hppt1->SetBinError(5,0.009838054);
   hppt1->SetBinError(6,0.005990909);
   hppt1->SetBinError(7,0.003490304);
   hppt1->SetBinError(8,0.0005238472);
   hppt1->SetMinimum(0);
   hppt1->SetMaximum(0.6308189);
   hppt1->SetEntries(4045);
   hppt1->SetStats(0);
   hppt1->SetLineWidth(2);
   hppt1->SetMarkerStyle(20);
   hppt1->GetXaxis()->SetNdivisions(505);
   hppt1->GetXaxis()->SetLabelFont(42);
   hppt1->GetXaxis()->SetLabelOffset(0.015);
   hppt1->GetXaxis()->SetLabelSize(0.05);
   hppt1->GetXaxis()->SetTitleSize(0.06);
   hppt1->GetXaxis()->SetTitleFont(42);
   hppt1->GetYaxis()->SetTitle("Arbitrary scale");
   hppt1->GetYaxis()->SetLabelFont(42);
   hppt1->GetYaxis()->SetLabelSize(0.05);
   hppt1->GetYaxis()->SetTitleSize(0.06);
   hppt1->GetYaxis()->SetTitleOffset(0.84);
   hppt1->GetYaxis()->SetTitleFont(42);
   hppt1->GetZaxis()->SetLabelFont(42);
   hppt1->GetZaxis()->SetLabelSize(0.05);
   hppt1->GetZaxis()->SetTitleSize(0.06);
   hppt1->GetZaxis()->SetTitleFont(42);
   hppt1->Draw("same");
   
   TH1D *hppt2 = new TH1D("hppt2","backward p_{T} [GeV/c]",10,0,15);
   hppt2->SetBinContent(1,0.2010103);
   hppt2->SetBinContent(2,0.5530506);
   hppt2->SetBinContent(3,0.1710386);
   hppt2->SetBinContent(4,0.04698366);
   hppt2->SetBinContent(5,0.01478805);
   hppt2->SetBinContent(6,0.01580656);
   hppt2->SetBinContent(7,-0.002677788);
   hppt2->SetBinError(1,0.1206234);
   hppt2->SetBinError(2,0.1675558);
   hppt2->SetBinError(3,0.06925909);
   hppt2->SetBinError(4,0.03131689);
   hppt2->SetBinError(5,0.01138702);
   hppt2->SetBinError(6,0.01262769);
   hppt2->SetBinError(7,0.002208978);
   hppt2->SetMinimum(0);
   hppt2->SetMaximum(0.6308189);
   hppt2->SetEntries(1975);
   hppt2->SetStats(0);
   hppt2->SetLineColor(2);
   hppt2->SetLineWidth(2);
   hppt2->SetMarkerColor(2);
   hppt2->SetMarkerStyle(21);
   hppt2->GetXaxis()->SetNdivisions(505);
   hppt2->GetXaxis()->SetLabelFont(42);
   hppt2->GetXaxis()->SetLabelOffset(0.015);
   hppt2->GetXaxis()->SetLabelSize(0.05);
   hppt2->GetXaxis()->SetTitleSize(0.06);
   hppt2->GetXaxis()->SetTitleFont(42);
   hppt2->GetYaxis()->SetTitle("Arbitrary scale");
   hppt2->GetYaxis()->SetLabelFont(42);
   hppt2->GetYaxis()->SetLabelSize(0.05);
   hppt2->GetYaxis()->SetTitleSize(0.06);
   hppt2->GetYaxis()->SetTitleOffset(0.84);
   hppt2->GetYaxis()->SetTitleFont(42);
   hppt2->GetZaxis()->SetLabelFont(42);
   hppt2->GetZaxis()->SetLabelSize(0.05);
   hppt2->GetZaxis()->SetTitleSize(0.06);
   hppt2->GetZaxis()->SetTitleFont(42);
   hppt2->Draw("same");
   
   TH1D *hppt3 = new TH1D("hppt3","forward MC p_{T} [GeV/c]",10,0,15);
   hppt3->SetBinContent(1,0.1922204);
   hppt3->SetBinContent(2,0.5310731);
   hppt3->SetBinContent(3,0.1822434);
   hppt3->SetBinContent(4,0.05818102);
   hppt3->SetBinContent(5,0.02192083);
   hppt3->SetBinContent(6,0.009051019);
   hppt3->SetBinContent(7,0.003641344);
   hppt3->SetBinContent(8,0.001413936);
   hppt3->SetBinContent(9,0.000254944);
   hppt3->SetBinError(1,0.0007498297);
   hppt3->SetBinError(2,0.001253414);
   hppt3->SetBinError(3,0.0007400045);
   hppt3->SetBinError(4,0.0004221168);
   hppt3->SetBinError(5,0.0002596176);
   hppt3->SetBinError(6,0.0001688383);
   hppt3->SetBinError(7,0.0001072529);
   hppt3->SetBinError(8,6.702573e-05);
   hppt3->SetBinError(9,2.78662e-05);
   hppt3->SetMinimum(0);
   hppt3->SetMaximum(0.6308189);
   hppt3->SetEntries(380895);
   hppt3->SetStats(0);
   hppt3->SetLineColor(4);
   hppt3->SetLineWidth(2);
   hppt3->SetMarkerColor(4);
   hppt3->SetMarkerStyle(22);
   hppt3->GetXaxis()->SetTitle("p_{T}(#mu^{+})");
   hppt3->GetXaxis()->SetNdivisions(505);
   hppt3->GetXaxis()->SetLabelFont(42);
   hppt3->GetXaxis()->SetLabelOffset(0.015);
   hppt3->GetXaxis()->SetLabelSize(0.05);
   hppt3->GetXaxis()->SetTitleSize(0.06);
   hppt3->GetXaxis()->SetTitleFont(42);
   hppt3->GetYaxis()->SetTitle("Arbitrary scale");
   hppt3->GetYaxis()->SetLabelFont(42);
   hppt3->GetYaxis()->SetLabelSize(0.05);
   hppt3->GetYaxis()->SetTitleSize(0.06);
   hppt3->GetYaxis()->SetTitleOffset(0.84);
   hppt3->GetYaxis()->SetTitleFont(42);
   hppt3->GetZaxis()->SetLabelFont(42);
   hppt3->GetZaxis()->SetLabelSize(0.05);
   hppt3->GetZaxis()->SetTitleSize(0.06);
   hppt3->GetZaxis()->SetTitleFont(42);
   hppt3->Draw("same");
   
   TLegend *leg = new TLegend(0.58,0.55,0.79,0.69,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(2);
   leg->SetFillColor(10);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("hppt1","pPb(Fwd)","lpe");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hppt2","pPb(Bwd)","lpe");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hppt3","pp MC","lpe");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(22);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   TLatex *   tex = new TLatex(0.58,0.78,"#splitline{#splitline{LHCb}{Preliminary}}{#scale[0.8]{pPb #sqrt{s_{NN}} = 5 TeV}}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
   pptCanvas->Modified();
   pptCanvas->cd();
   pptCanvas->SetSelected(pptCanvas);
}
