{
//=========Macro generated from canvas: pvx2Canvas/pt Canvas
//=========  (Tue Sep 15 00:49:19 2015) by ROOT version5.34/18
   TCanvas *pvx2Canvas = new TCanvas("pvx2Canvas", "pt Canvas",55,52,500,400);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   pvx2Canvas->Range(0.2839506,-0.07295659,1.82716,0.4481619);
   pvx2Canvas->SetFillColor(0);
   pvx2Canvas->SetBorderMode(0);
   pvx2Canvas->SetBorderSize(2);
   pvx2Canvas->SetTickx(1);
   pvx2Canvas->SetTicky(1);
   pvx2Canvas->SetLeftMargin(0.14);
   pvx2Canvas->SetRightMargin(0.05);
   pvx2Canvas->SetTopMargin(0.07);
   pvx2Canvas->SetBottomMargin(0.14);
   pvx2Canvas->SetFrameLineWidth(2);
   pvx2Canvas->SetFrameBorderMode(0);
   pvx2Canvas->SetFrameLineWidth(2);
   pvx2Canvas->SetFrameBorderMode(0);
   
   TH1D *hpvx23 = new TH1D("hpvx23","forward MC Y [GeV/c]",10,0.5,1.75);
   hpvx23->SetBinContent(0,0.009490871);
   hpvx23->SetBinContent(1,0.01379515);
   hpvx23->SetBinContent(2,0.03254776);
   hpvx23->SetBinContent(3,0.07486145);
   hpvx23->SetBinContent(4,0.1585242);
   hpvx23->SetBinContent(5,0.2418538);
   hpvx23->SetBinContent(6,0.233077);
   hpvx23->SetBinContent(7,0.1424904);
   hpvx23->SetBinContent(8,0.06475398);
   hpvx23->SetBinContent(9,0.02671675);
   hpvx23->SetBinContent(10,0.01137939);
   hpvx23->SetBinContent(11,0.01183355);
   hpvx23->SetBinError(0,0.0002854401);
   hpvx23->SetBinError(1,0.0003190201);
   hpvx23->SetBinError(2,0.0004857035);
   hpvx23->SetBinError(3,0.0007369636);
   hpvx23->SetBinError(4,0.001195561);
   hpvx23->SetBinError(5,0.001628563);
   hpvx23->SetBinError(6,0.001730483);
   hpvx23->SetBinError(7,0.001393756);
   hpvx23->SetBinError(8,0.0009364199);
   hpvx23->SetBinError(9,0.000588609);
   hpvx23->SetBinError(10,0.0003583936);
   hpvx23->SetBinError(11,0.0003599723);
   hpvx23->SetMinimum(0);
   hpvx23->SetMaximum(0.4116836);
   hpvx23->SetEntries(379360);
   hpvx23->SetStats(0);
   hpvx23->SetLineColor(4);
   hpvx23->SetLineWidth(2);
   hpvx23->SetMarkerColor(4);
   hpvx23->SetMarkerStyle(22);
   hpvx23->GetXaxis()->SetTitle("OWNPV#chi^{2}/nodf");
   hpvx23->GetXaxis()->SetNdivisions(505);
   hpvx23->GetXaxis()->SetLabelFont(42);
   hpvx23->GetXaxis()->SetLabelOffset(0.015);
   hpvx23->GetXaxis()->SetLabelSize(0.05);
   hpvx23->GetXaxis()->SetTitleSize(0.06);
   hpvx23->GetXaxis()->SetTitleFont(42);
   hpvx23->GetYaxis()->SetTitle("Arbitrary scale");
   hpvx23->GetYaxis()->SetLabelFont(42);
   hpvx23->GetYaxis()->SetLabelSize(0.05);
   hpvx23->GetYaxis()->SetTitleSize(0.06);
   hpvx23->GetYaxis()->SetTitleOffset(0.84);
   hpvx23->GetYaxis()->SetTitleFont(42);
   hpvx23->GetZaxis()->SetLabelFont(42);
   hpvx23->GetZaxis()->SetLabelSize(0.05);
   hpvx23->GetZaxis()->SetTitleSize(0.06);
   hpvx23->GetZaxis()->SetTitleFont(42);
   hpvx23->Draw("");
   
   TH1D *hpvx21 = new TH1D("hpvx21","forward Y [GeV/c]",10,0.5,1.75);
   hpvx21->SetBinContent(0,0.001870844);
   hpvx21->SetBinContent(1,0.005257753);
   hpvx21->SetBinContent(2,0.01065318);
   hpvx21->SetBinContent(3,0.06666426);
   hpvx21->SetBinContent(4,0.1682672);
   hpvx21->SetBinContent(5,0.3366295);
   hpvx21->SetBinContent(6,0.2929788);
   hpvx21->SetBinContent(7,0.07347243);
   hpvx21->SetBinContent(8,0.0292923);
   hpvx21->SetBinContent(9,0.007341933);
   hpvx21->SetBinContent(10,0.009442604);
   hpvx21->SetBinContent(11,0.003834058);
   hpvx21->SetBinError(0,0.001870844);
   hpvx21->SetBinError(1,0.003873308);
   hpvx21->SetBinError(2,0.00756956);
   hpvx21->SetBinError(3,0.02068444);
   hpvx21->SetBinError(4,0.03826476);
   hpvx21->SetBinError(5,0.05399505);
   hpvx21->SetBinError(6,0.04886133);
   hpvx21->SetBinError(7,0.02470676);
   hpvx21->SetBinError(8,0.01113257);
   hpvx21->SetBinError(9,0.005887159);
   hpvx21->SetBinError(10,0.005186407);
   hpvx21->SetBinError(11,0.003467335);
   hpvx21->SetMinimum(0);
   hpvx21->SetMaximum(0.4116836);
   hpvx21->SetEntries(4045);
   hpvx21->SetStats(0);
   hpvx21->SetLineWidth(2);
   hpvx21->SetMarkerStyle(20);
   hpvx21->GetXaxis()->SetNdivisions(505);
   hpvx21->GetXaxis()->SetLabelFont(42);
   hpvx21->GetXaxis()->SetLabelOffset(0.015);
   hpvx21->GetXaxis()->SetLabelSize(0.05);
   hpvx21->GetXaxis()->SetTitleSize(0.06);
   hpvx21->GetXaxis()->SetTitleFont(42);
   hpvx21->GetYaxis()->SetTitle("Arbitrary scale");
   hpvx21->GetYaxis()->SetLabelFont(42);
   hpvx21->GetYaxis()->SetLabelSize(0.05);
   hpvx21->GetYaxis()->SetTitleSize(0.06);
   hpvx21->GetYaxis()->SetTitleOffset(0.84);
   hpvx21->GetYaxis()->SetTitleFont(42);
   hpvx21->GetZaxis()->SetLabelFont(42);
   hpvx21->GetZaxis()->SetLabelSize(0.05);
   hpvx21->GetZaxis()->SetTitleSize(0.06);
   hpvx21->GetZaxis()->SetTitleFont(42);
   hpvx21->Draw("same");
   
   TH1D *hpvx22 = new TH1D("hpvx22","backward Y [GeV/c]",10,0.5,1.75);
   hpvx22->SetBinContent(0,0.001079428);
   hpvx22->SetBinContent(1,-0.001086647);
   hpvx22->SetBinContent(2,0.003771785);
   hpvx22->SetBinContent(3,0.007972917);
   hpvx22->SetBinContent(4,0.1416889);
   hpvx22->SetBinContent(5,0.3396673);
   hpvx22->SetBinContent(6,0.3715101);
   hpvx22->SetBinContent(7,0.1608823);
   hpvx22->SetBinContent(8,-0.03022579);
   hpvx22->SetBinContent(9,0.005819214);
   hpvx22->SetBinContent(11,0.006643465);
   hpvx22->SetBinError(0,0.001079428);
   hpvx22->SetBinError(1,0.004331795);
   hpvx22->SetBinError(2,0.008692736);
   hpvx22->SetBinError(3,0.03184828);
   hpvx22->SetBinError(4,0.08376213);
   hpvx22->SetBinError(5,0.131384);
   hpvx22->SetBinError(6,0.1334926);
   hpvx22->SetBinError(7,0.07650566);
   hpvx22->SetBinError(8,0.01681578);
   hpvx22->SetBinError(9,0.007976227);
   hpvx22->SetBinError(11,0.006643465);
   hpvx22->SetMinimum(0);
   hpvx22->SetMaximum(0.4116836);
   hpvx22->SetEntries(1975);
   hpvx22->SetStats(0);
   hpvx22->SetLineColor(2);
   hpvx22->SetLineWidth(2);
   hpvx22->SetMarkerColor(2);
   hpvx22->SetMarkerStyle(21);
   hpvx22->GetXaxis()->SetNdivisions(505);
   hpvx22->GetXaxis()->SetLabelFont(42);
   hpvx22->GetXaxis()->SetLabelOffset(0.015);
   hpvx22->GetXaxis()->SetLabelSize(0.05);
   hpvx22->GetXaxis()->SetTitleSize(0.06);
   hpvx22->GetXaxis()->SetTitleFont(42);
   hpvx22->GetYaxis()->SetTitle("Arbitrary scale");
   hpvx22->GetYaxis()->SetLabelFont(42);
   hpvx22->GetYaxis()->SetLabelSize(0.05);
   hpvx22->GetYaxis()->SetTitleSize(0.06);
   hpvx22->GetYaxis()->SetTitleOffset(0.84);
   hpvx22->GetYaxis()->SetTitleFont(42);
   hpvx22->GetZaxis()->SetLabelFont(42);
   hpvx22->GetZaxis()->SetLabelSize(0.05);
   hpvx22->GetZaxis()->SetTitleSize(0.06);
   hpvx22->GetZaxis()->SetTitleFont(42);
   hpvx22->Draw("same");
   
   TH1D *hpvx23 = new TH1D("hpvx23","forward MC Y [GeV/c]",10,0.5,1.75);
   hpvx23->SetBinContent(0,0.009490871);
   hpvx23->SetBinContent(1,0.01379515);
   hpvx23->SetBinContent(2,0.03254776);
   hpvx23->SetBinContent(3,0.07486145);
   hpvx23->SetBinContent(4,0.1585242);
   hpvx23->SetBinContent(5,0.2418538);
   hpvx23->SetBinContent(6,0.233077);
   hpvx23->SetBinContent(7,0.1424904);
   hpvx23->SetBinContent(8,0.06475398);
   hpvx23->SetBinContent(9,0.02671675);
   hpvx23->SetBinContent(10,0.01137939);
   hpvx23->SetBinContent(11,0.01183355);
   hpvx23->SetBinError(0,0.0002854401);
   hpvx23->SetBinError(1,0.0003190201);
   hpvx23->SetBinError(2,0.0004857035);
   hpvx23->SetBinError(3,0.0007369636);
   hpvx23->SetBinError(4,0.001195561);
   hpvx23->SetBinError(5,0.001628563);
   hpvx23->SetBinError(6,0.001730483);
   hpvx23->SetBinError(7,0.001393756);
   hpvx23->SetBinError(8,0.0009364199);
   hpvx23->SetBinError(9,0.000588609);
   hpvx23->SetBinError(10,0.0003583936);
   hpvx23->SetBinError(11,0.0003599723);
   hpvx23->SetMinimum(0);
   hpvx23->SetMaximum(0.4116836);
   hpvx23->SetEntries(379360);
   hpvx23->SetStats(0);
   hpvx23->SetLineColor(4);
   hpvx23->SetLineWidth(2);
   hpvx23->SetMarkerColor(4);
   hpvx23->SetMarkerStyle(22);
   hpvx23->GetXaxis()->SetTitle("OWNPV#chi^{2}/nodf");
   hpvx23->GetXaxis()->SetNdivisions(505);
   hpvx23->GetXaxis()->SetLabelFont(42);
   hpvx23->GetXaxis()->SetLabelOffset(0.015);
   hpvx23->GetXaxis()->SetLabelSize(0.05);
   hpvx23->GetXaxis()->SetTitleSize(0.06);
   hpvx23->GetXaxis()->SetTitleFont(42);
   hpvx23->GetYaxis()->SetTitle("Arbitrary scale");
   hpvx23->GetYaxis()->SetLabelFont(42);
   hpvx23->GetYaxis()->SetLabelSize(0.05);
   hpvx23->GetYaxis()->SetTitleSize(0.06);
   hpvx23->GetYaxis()->SetTitleOffset(0.84);
   hpvx23->GetYaxis()->SetTitleFont(42);
   hpvx23->GetZaxis()->SetLabelFont(42);
   hpvx23->GetZaxis()->SetLabelSize(0.05);
   hpvx23->GetZaxis()->SetTitleSize(0.06);
   hpvx23->GetZaxis()->SetTitleFont(42);
   hpvx23->Draw("same");
   
   TLegend *leg = new TLegend(0.68,0.75,0.89,0.89,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(2);
   leg->SetFillColor(10);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("hpvx21","pPb(Fwd)","lpe");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hpvx22","pPb(Bwd)","lpe");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hpvx23","pp MC","lpe");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(22);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   TLatex *   tex = new TLatex(0.18,0.78,"#splitline{#splitline{LHCb}{Preliminary}}{#scale[0.8]{pPb #sqrt{s_{NN}} = 5 TeV}}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
   pvx2Canvas->Modified();
   pvx2Canvas->cd();
   pvx2Canvas->SetSelected(pvx2Canvas);
}
