//#include <lhcbStyle.h>
#include <algorithm>
void Draw(TH1D *hpt1, TH1D *hpt2, TH1D *hpt3);
void Draw2(TH1D *hpt1, TH1D *hpt2, TH1D *hpt3);

int which_bin(int x, double* bin)
{
    for (int n = 0; n < 4; n++)
    {
        if (bin[n] <= x && x < bin[n+1])
            return n+1;
    }
    return 0;
}

void correct_pA()
{

	gROOT->ProcessLine(".L ../lhcbStyle.C");
    lhcbStyle();
    TH1::SetDefaultSumw2();

    TChain *chain1 = new TChain("DecayTree");
    chain1->Add("../RealVariablepA.root/DecayTree");
    TChain *chain2 = new TChain("DecayTree");
    chain2->Add("../RealVariableAp.root/DecayTree");
    TChain *chain3 = new TChain("DecayTree");
    chain3->Add("../RecEff/pADtrackMC_psip.root/DecayTree");

//    TFile *f3 = new TFile("../RecEff/pADtrackMC_psip.root");
//    TTree* chain3 = (TTree*)f3->Get("DecayTree");

    const int ntkBins = 4;
    double bin[ntkBins+1]={0, 100, 200, 300, 1000};
    TCanvas *ntrkCanvas = new TCanvas("ntrkCanvas", "pt Canvas", 0, 0, 500, 400);
    ntrkCanvas->cd();
    TH1D * hntrk1 = new TH1D("hntrk1", "forward Y [GeV/c]", ntkBins, bin);
    TH1D * hntrk2 = new TH1D("hntrk2", "backward Y [GeV/c]", ntkBins, bin);
    TH1D * hntrk3 = new TH1D("hntrk3", "forward MC Y [GeV/c]", ntkBins, bin);
    TH1D * hntrk4 = new TH1D("hntrk4", "", ntkBins, bin);
    chain1->Draw("nTracks >> hntrk1", "nsig_sw");
    chain2->Draw("nTracks >> hntrk2", "nsig_sw");
    chain3->Draw("nTracks >> hntrk3", "daughterP_ProbNNmu >= 0.6 && daughterM_ProbNNmu >= 0.6");
    hntrk1->Scale(1/hntrk1->Integral());
    hntrk2->Scale(1/hntrk2->Integral());
    hntrk3->Scale(1/hntrk3->Integral());
    hntrk3->GetXaxis()->SetTitle("Number of tracks");
    ntrkCanvas->SaveAs("nTracks.png");
    ntrkCanvas->SaveAs("nTracks.pdf");
    ntrkCanvas->SaveAs("nTracks.eps");
    ntrkCanvas->SaveAs("nTracks.C");

    TFile *file = new TFile("pA_corrected_psip.root", "recreate");
    TTree *tree = (TTree*)chain3->CloneTree(0);
    Double_t w;
    Double_t nn;
    tree->Branch("weight", &w, "weight/D");
    tree->Branch("nn", &nn, "nn/D");

    Int_t           nt;
    chain3->SetBranchAddress("nTracks", &nt);

    int tt;
    tree->Branch("tt", &tt, "tt/I");

    int n = chain3->GetEntries();
    for (int i = 0; i < n; i++)
    {
        chain3->LoadTree(i);
        chain3->GetEntry(i);
        nn = which_bin(nt, bin);
        if (which_bin(nt, bin) == 0) w = 0;
        else
        w = hntrk1->GetBinContent(which_bin(nt, bin))/hntrk3->GetBinContent(which_bin(nt, bin));
        //w = chain3->GetBinContent(which_bin(nt, bin));
        //w = which_bin(nt, bin);
        //if (w == 0) cout << "wrong " << nt << endl;
        if (nt < 100 && which_bin(nt, bin) == 2) cout << "###############" << which_bin(nt, bin) << " " << nt << endl;
        if (i % 1000 == 0) cout << nt << " " << which_bin(nt, bin) << " " << w << endl;

        tt = nt;
        tree->Fill();
    }
    tree->Write();
    tree->Draw("nTracks");

//    tree->Draw("nTracks >> hntrk4", "weight");
//    Draw(hntrk1, hntrk2, hntrk3, hntrk4);
    file->Close();
}

void Draw(TH1D *hpt1, TH1D *hpt2, TH1D *hpt3, TH1D *hpt4)
{
    TLatex *  myTex= new TLatex(0.58,0.78,"#splitline{#splitline{LHCb}{Preliminary}}{#scale[0.8]{pPb #sqrt{s_{NN}} = 5 TeV}}");
    myTex->SetNDC(kTRUE);
    myTex->SetTextSize(0.05);
    myTex->SetTextAlign(11);

    hpt1->GetYaxis()->SetTitle("Arbitrary scale");
    hpt2->GetYaxis()->SetTitle("Arbitrary scale");
    hpt3->GetYaxis()->SetTitle("Arbitrary scale");
    hpt4->GetYaxis()->SetTitle("Arbitrary scale");

    hpt1->Scale(1 / hpt1->Integral());
    hpt1->Draw("same");
    hpt2->Scale(1 / hpt2->Integral());
    hpt2->Draw("same");
    hpt3->Scale(1 / hpt3->Integral());
    hpt3->Draw("same");
    hpt4->Scale(1 / hpt4->Integral());
    hpt4->Draw("same");
    hpt1->SetLineColor(1);
    hpt2->SetLineColor(2);
    hpt3->SetLineColor(4);
    hpt4->SetLineColor(3);
    hpt1->SetMarkerColor(1);
    hpt2->SetMarkerColor(2);
    hpt3->SetMarkerColor(4);
    hpt4->SetMarkerColor(3);
    hpt1->SetMarkerStyle(20);
    hpt2->SetMarkerStyle(21);
    hpt3->SetMarkerStyle(22);
    hpt4->SetMarkerStyle(23);

    hpt1->GetYaxis()->SetTitleOffset(0.84);
    hpt2->GetYaxis()->SetTitleOffset(0.84);
    hpt3->GetYaxis()->SetTitleOffset(0.84);
    hpt4->GetYaxis()->SetTitleOffset(0.84);

    Double_t top = -100000;
    Double_t bottom = 1000000;
    top = max(top, hpt1->GetMaximum());
    top = max(top, hpt2->GetMaximum());
    top = max(top, hpt3->GetMaximum());
    top = max(top, hpt4->GetMaximum());
    bottom = min(bottom, hpt1->GetMinimum());
    bottom = min(bottom, hpt2->GetMinimum());
    bottom = min(bottom, hpt3->GetMinimum());
    bottom = min(bottom, hpt4->GetMinimum());

    hpt1->GetYaxis()->SetRangeUser(0, (top+bottom)/2 + (top-bottom)*0.6);
    hpt2->GetYaxis()->SetRangeUser(0, (top+bottom)/2 + (top-bottom)*0.6);
    hpt3->GetYaxis()->SetRangeUser(0, (top+bottom)/2 + (top-bottom)*0.6);
    hpt4->GetYaxis()->SetRangeUser(0, (top+bottom)/2 + (top-bottom)*0.6);

    TLegend *legend =new TLegend(0.58,0.55,0.79,0.69);
    legend->AddEntry(hpt1,"pPb(Fwd)","lpe");
    legend->AddEntry(hpt2,"pPb(Bwd)","lpe");
    legend->AddEntry(hpt3,"pp MC","lpe");
    //legend->AddEntry(hpt,"pp MC","lpe");
    legend->SetBorderSize(0);
    legend->Draw();
    myTex->Draw();
}


