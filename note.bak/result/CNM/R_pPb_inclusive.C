{
//=========Macro generated from canvas: myCi/
//=========  (Mon Nov 30 19:57:44 2015) by ROOT version5.34/18
   TCanvas *myCi = new TCanvas("myCi", "",55,52,500,400);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   myCi->Range(-6.728395,-0.3544304,5.617284,2.177215);
   myCi->SetFillColor(0);
   myCi->SetBorderMode(0);
   myCi->SetBorderSize(2);
   myCi->SetTickx(1);
   myCi->SetTicky(1);
   myCi->SetLeftMargin(0.14);
   myCi->SetRightMargin(0.05);
   myCi->SetTopMargin(0.07);
   myCi->SetBottomMargin(0.14);
   myCi->SetFrameLineWidth(2);
   myCi->SetFrameBorderMode(0);
   myCi->SetFrameLineWidth(2);
   myCi->SetFrameBorderMode(0);
   
   TGraphErrors *gre = new TGraphErrors(2);
   gre->SetName("Graph0");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetLineWidth(3);
   gre->SetMarkerStyle(20);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,3.25,0.472878);
   gre->SetPointError(0,0.75,0.0667562);
   gre->SetPoint(1,-3.25,0.510656);
   gre->SetPointError(1,0.75,0.14579);
   
   TH1F *Graph_Graph9 = new TH1F("Graph_Graph9","Graph",100,-5,5);
   Graph_Graph9->SetMinimum(0);
   Graph_Graph9->SetMaximum(2);
   Graph_Graph9->SetDirectory(0);
   Graph_Graph9->SetStats(0);
   Graph_Graph9->SetLineWidth(2);
   Graph_Graph9->SetMarkerStyle(20);
   Graph_Graph9->SetMarkerSize(1.5);
   Graph_Graph9->GetXaxis()->SetTitle("#font[12]{y}");
   Graph_Graph9->GetXaxis()->SetNdivisions(505);
   Graph_Graph9->GetXaxis()->SetLabelFont(132);
   Graph_Graph9->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph9->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph9->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph9->GetXaxis()->SetTitleFont(132);
   Graph_Graph9->GetYaxis()->SetTitle("#font[12]{R}_{pPb}");
   Graph_Graph9->GetYaxis()->SetLabelFont(132);
   Graph_Graph9->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph9->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph9->GetYaxis()->SetTitleFont(132);
   Graph_Graph9->GetZaxis()->SetLabelFont(42);
   Graph_Graph9->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph9->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph9->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph9);
   
   gre->Draw("ap");
   
   gre = new TGraphErrors(2);
   gre->SetName("Graph1");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);

   Int_t ci;   // for color index setting
   ci = TColor::GetColor("#ff0000");
   gre->SetLineColor(ci);
   gre->SetLineWidth(3);

   ci = TColor::GetColor("#ff0000");
   gre->SetMarkerColor(ci);
   gre->SetMarkerStyle(25);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,-3.25,0.93);
   gre->SetPointError(0,0.75,0.026);
   gre->SetPoint(1,3.25,0.633);
   gre->SetPointError(1,0.75,0.007);
   
   TH1F *Graph_Graph10 = new TH1F("Graph_Graph10","Graph",100,-4.8,4.8);
   Graph_Graph10->SetMinimum(0.593);
   Graph_Graph10->SetMaximum(0.989);
   Graph_Graph10->SetDirectory(0);
   Graph_Graph10->SetStats(0);
   Graph_Graph10->SetLineWidth(2);
   Graph_Graph10->SetMarkerStyle(20);
   Graph_Graph10->SetMarkerSize(1.5);
   Graph_Graph10->GetXaxis()->SetNdivisions(505);
   Graph_Graph10->GetXaxis()->SetLabelFont(42);
   Graph_Graph10->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph10->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph10->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph10->GetXaxis()->SetTitleFont(42);
   Graph_Graph10->GetYaxis()->SetLabelFont(42);
   Graph_Graph10->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph10->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph10->GetYaxis()->SetTitleFont(42);
   Graph_Graph10->GetZaxis()->SetLabelFont(42);
   Graph_Graph10->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph10->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph10->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph10);
   
   gre->Draw("p");
   
   gre = new TGraphErrors(2);
   gre->SetName("Graph2");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);

   ci = TColor::GetColor("#ff0000");
   gre->SetLineColor(ci);
   gre->SetLineWidth(3);

   ci = TColor::GetColor("#ff0000");
   gre->SetMarkerColor(ci);
   gre->SetMarkerStyle(25);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,-3.25,0.93);
   gre->SetPointError(0,0.75,0.071);
   gre->SetPoint(1,3.25,0.633);
   gre->SetPointError(1,0.75,0.044);
   gre->Draw("z");
   
   gre = new TGraphErrors(2);
   gre->SetName("Graph3");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineWidth(3);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(24);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,-3.71,0.58);
   gre->SetPointError(0,0.75,0.13);
   gre->SetPoint(1,2.78,0.47);
   gre->SetPointError(1,0.75,0.1);
   
   TH1F *Graph_Graph11 = new TH1F("Graph_Graph11","Graph",100,-5.259,4.329);
   Graph_Graph11->SetMinimum(0.336);
   Graph_Graph11->SetMaximum(0.744);
   Graph_Graph11->SetDirectory(0);
   Graph_Graph11->SetStats(0);
   Graph_Graph11->SetLineWidth(2);
   Graph_Graph11->SetMarkerStyle(20);
   Graph_Graph11->SetMarkerSize(1.5);
   Graph_Graph11->GetXaxis()->SetNdivisions(505);
   Graph_Graph11->GetXaxis()->SetLabelFont(42);
   Graph_Graph11->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph11->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph11->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph11->GetXaxis()->SetTitleFont(42);
   Graph_Graph11->GetYaxis()->SetLabelFont(42);
   Graph_Graph11->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph11->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph11->GetYaxis()->SetTitleFont(42);
   Graph_Graph11->GetZaxis()->SetLabelFont(42);
   Graph_Graph11->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph11->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph11->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph11);
   
   gre->Draw("p");
   
   gre = new TGraphErrors(2);
   gre->SetName("Graph4");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetLineWidth(3);
   gre->SetMarkerStyle(20);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,3.25,0.472878);
   gre->SetPointError(0,0.75,0.0875008);
   gre->SetPoint(1,-3.25,0.510656);
   gre->SetPointError(1,0.75,0.1592317);
   gre->Draw("z");
   
   gre = new TGraphErrors(2);
   gre->SetName("Graph0");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetLineWidth(3);
   gre->SetMarkerStyle(20);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,3.25,0.472878);
   gre->SetPointError(0,0.75,0.0667562);
   gre->SetPoint(1,-3.25,0.510656);
   gre->SetPointError(1,0.75,0.14579);
   
   TH1F *Graph_Graph_Graph912 = new TH1F("Graph_Graph_Graph912","Graph",100,-5,5);
   Graph_Graph_Graph912->SetMinimum(0);
   Graph_Graph_Graph912->SetMaximum(2);
   Graph_Graph_Graph912->SetDirectory(0);
   Graph_Graph_Graph912->SetStats(0);
   Graph_Graph_Graph912->SetLineWidth(2);
   Graph_Graph_Graph912->SetMarkerStyle(20);
   Graph_Graph_Graph912->SetMarkerSize(1.5);
   Graph_Graph_Graph912->GetXaxis()->SetTitle("#font[12]{y}");
   Graph_Graph_Graph912->GetXaxis()->SetNdivisions(505);
   Graph_Graph_Graph912->GetXaxis()->SetLabelFont(132);
   Graph_Graph_Graph912->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph_Graph912->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph_Graph912->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph_Graph912->GetXaxis()->SetTitleFont(132);
   Graph_Graph_Graph912->GetYaxis()->SetTitle("#font[12]{R}_{pPb}");
   Graph_Graph_Graph912->GetYaxis()->SetLabelFont(132);
   Graph_Graph_Graph912->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph_Graph912->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph_Graph912->GetYaxis()->SetTitleFont(132);
   Graph_Graph_Graph912->GetZaxis()->SetLabelFont(42);
   Graph_Graph_Graph912->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph_Graph912->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph_Graph912->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph_Graph912);
   
   gre->Draw("p");
   
   TLegend *leg = new TLegend(0.45,0.65,0.92,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(132);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(3);
   leg->SetFillColor(10);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("Graph0","LHCb, inclusive #font[12]{#psi}(2#font[12]{S})","ple");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(3);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(132);
   entry=leg->AddEntry("Graph2","LHCb, inclusive #font[12]{J/#psi} ","ple");

   ci = TColor::GetColor("#ff0000");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(3);

   ci = TColor::GetColor("#ff0000");
   entry->SetMarkerColor(ci);
   entry->SetMarkerStyle(25);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(132);
   entry=leg->AddEntry("Graph3","ALICE, inclusive #font[12]{#psi}(2#font[12]{S})","ple");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(3);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(24);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(132);
   leg->Draw();
   TLatex *   tex = new TLatex(0.18,0.78,"#splitline{LHCb}{#scale[1.0]{pPb #font[12]{#sqrt{s_{NN}}} = 5 TeV}}");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetLineWidth(2);
   tex->Draw();
   TLine *line = new TLine(-5,1,5,1);

   ci = TColor::GetColor("#cccccc");
   line->SetLineColor(ci);
   line->SetLineStyle(2);
   line->SetLineWidth(3);
   line->Draw();
      tex = new TLatex(0.18,0.18,"#font[12]{p}_{T} < 14 GeV/#font[12]{c}");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetTextSize(0.045);
   tex->SetLineWidth(2);
   tex->Draw();
   myCi->Modified();
   myCi->cd();
   myCi->SetSelected(myCi);
}
