{
//=========Macro generated from canvas: myCB/myCB
//=========  (Mon Nov 30 20:01:45 2015) by ROOT version5.34/18
   TCanvas *myCB = new TCanvas("myCB", "myCB",55,52,500,400);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   myCB->Range(-0.8641975,-1.267089,5.308642,2.783544);
   myCB->SetFillColor(0);
   myCB->SetBorderMode(0);
   myCB->SetBorderSize(2);
   myCB->SetTickx(1);
   myCB->SetTicky(1);
   myCB->SetLeftMargin(0.14);
   myCB->SetRightMargin(0.05);
   myCB->SetTopMargin(0.07);
   myCB->SetBottomMargin(0.14);
   myCB->SetFrameLineWidth(2);
   myCB->SetFrameBorderMode(0);
   myCB->SetFrameLineWidth(2);
   myCB->SetFrameBorderMode(0);
   
   TGraphErrors *gre = new TGraphErrors(1);
   gre->SetName("");
   gre->SetTitle("");
   gre->SetFillColor(1);
   gre->SetLineWidth(2);
   gre->SetMarkerStyle(20);
   gre->SetPoint(0,3.25,0.93);
   gre->SetPointError(0,0.75,0.26);
   
   TH1F *Graph_Graph1 = new TH1F("Graph_Graph1","",100,0,5);
   Graph_Graph1->SetMinimum(-0.7);
   Graph_Graph1->SetMaximum(2.5);
   Graph_Graph1->SetDirectory(0);
   Graph_Graph1->SetStats(0);
   Graph_Graph1->SetLineWidth(2);
   Graph_Graph1->SetMarkerStyle(20);
   Graph_Graph1->GetXaxis()->SetTitle("|#font[12]{y}|");
   Graph_Graph1->GetXaxis()->SetNdivisions(505);
   Graph_Graph1->GetXaxis()->SetLabelFont(132);
   Graph_Graph1->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph1->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph1->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph1->GetXaxis()->SetTitleFont(132);
   Graph_Graph1->GetYaxis()->SetTitle("#font[12]{R}_{FB}");
   Graph_Graph1->GetYaxis()->SetLabelFont(132);
   Graph_Graph1->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph1->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph1->GetYaxis()->SetTitleFont(132);
   Graph_Graph1->GetZaxis()->SetLabelFont(42);
   Graph_Graph1->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph1->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph1->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph1);
   
   gre->Draw("ap");
   TLine *line = new TLine(0,1,5,1);

   Int_t ci;   // for color index setting
   ci = TColor::GetColor("#cccccc");
   line->SetLineColor(ci);
   line->SetLineStyle(2);
   line->SetLineWidth(2);
   line->Draw();
   TLatex *   tex = new TLatex(0.18,0.78,"#splitline{LHCb}{#scale[1.0]{pPb #font[12]{#sqrt{s_{NN}}} = 5 TeV}}");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetLineWidth(2);
   tex->Draw();
   
   TLegend *leg = new TLegend(0.17,0.16,0.56,0.41,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(132);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(2);
   leg->SetFillColor(10);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("TPolyLine","EPS09 LO","f");
   entry->SetFillColor(20);
   entry->SetFillStyle(3008);
   entry->SetLineColor(20);
   entry->SetLineStyle(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   entry=leg->AddEntry("TPolyLine","EPS09 NLO","f");
   entry->SetFillColor(7);
   entry->SetFillStyle(3007);
   entry->SetLineColor(7);
   entry->SetLineStyle(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   entry=leg->AddEntry("Graph","nDSg LO","L");

   ci = TColor::GetColor("#ff9900");
   entry->SetLineColor(ci);
   entry->SetLineStyle(9);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   entry=leg->AddEntry("Graph","E. loss","L");

   ci = TColor::GetColor("#00cc00");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   entry=leg->AddEntry("Graph","E. loss + EPS09 NLO","L");
   entry->SetLineColor(4);
   entry->SetLineStyle(5);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   leg->Draw();
   
   leg = new TLegend(0.58,0.78,0.87,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(132);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(2);
   leg->SetFillColor(10);
   leg->SetFillStyle(1001);
   entry=leg->AddEntry("","Inclusive #font[12]{#psi}(2#font[12]{S}) ","ple");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   entry=leg->AddEntry("","Inclusive #font[12]{J/#psi} ","ple");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   leg->Draw();
      tex = new TLatex(0.61,0.72,"#font[12]{p}_{T} < 14 GeV/#font[12]{c}");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetTextSize(0.045);
   tex->SetLineWidth(2);
   tex->Draw();
   
   Double_t *dum = 0;
   TPolyLine *pline = new TPolyLine(20,dum,dum,"");
   pline->SetFillColor(20);
   pline->SetFillStyle(3008);
   pline->SetLineColor(20);
   pline->SetLineWidth(0);
   pline->SetPoint(0,0.25,0.9715121);
   pline->SetPoint(1,0.75,0.9046401);
   pline->SetPoint(2,1.25,0.8377343);
   pline->SetPoint(3,1.75,0.7571444);
   pline->SetPoint(4,2.25,0.6892006);
   pline->SetPoint(5,2.75,0.6108534);
   pline->SetPoint(6,3.25,0.5377313);
   pline->SetPoint(7,3.75,0.4632769);
   pline->SetPoint(8,4.25,0.4242153);
   pline->SetPoint(9,4.75,0.3996483);
   pline->SetPoint(10,4.75,0.7830931);
   pline->SetPoint(11,4.25,0.7978808);
   pline->SetPoint(12,3.75,0.8167672);
   pline->SetPoint(13,3.25,0.850109);
   pline->SetPoint(14,2.75,0.8888268);
   pline->SetPoint(15,2.25,0.8992447);
   pline->SetPoint(16,1.75,0.9268537);
   pline->SetPoint(17,1.25,0.9573398);
   pline->SetPoint(18,0.75,0.9731721);
   pline->SetPoint(19,0.25,0.9984811);
   pline->Draw("fsame");
   
   Double_t *dum = 0;
   pline = new TPolyLine(20,dum,dum,"");
   pline->SetFillColor(7);
   pline->SetFillStyle(3007);
   pline->SetLineColor(7);
   pline->SetLineWidth(0);
   pline->SetPoint(0,0.25,0.996111);
   pline->SetPoint(1,0.75,0.9867859);
   pline->SetPoint(2,1.25,0.9766564);
   pline->SetPoint(3,1.75,0.964958);
   pline->SetPoint(4,2.25,0.9529844);
   pline->SetPoint(5,2.75,0.9370291);
   pline->SetPoint(6,3.25,0.916802);
   pline->SetPoint(7,3.75,0.895918);
   pline->SetPoint(8,4.25,0.8720484);
   pline->SetPoint(9,4.75,0.8624567);
   pline->SetPoint(10,4.75,0.5979172);
   pline->SetPoint(11,4.25,0.6240859);
   pline->SetPoint(12,3.75,0.6871921);
   pline->SetPoint(13,3.25,0.753261);
   pline->SetPoint(14,2.75,0.8152622);
   pline->SetPoint(15,2.25,0.863871);
   pline->SetPoint(16,1.75,0.9009373);
   pline->SetPoint(17,1.25,0.9321041);
   pline->SetPoint(18,0.75,0.9602489);
   pline->SetPoint(19,0.25,0.9872034);
   pline->Draw("fsame");
   
   TGraph *graph = new TGraph(10);
   graph->SetName("Graph0");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);

   ci = TColor::GetColor("#ff9900");
   graph->SetLineColor(ci);
   graph->SetLineStyle(9);
   graph->SetLineWidth(2);
   graph->SetMarkerColor(6);
   graph->SetMarkerStyle(20);
   graph->SetPoint(0,0.25,0.9684128445);
   graph->SetPoint(1,0.75,0.9511561609);
   graph->SetPoint(2,1.25,0.9371264252);
   graph->SetPoint(3,1.75,0.9047968225);
   graph->SetPoint(4,2.25,0.863847639);
   graph->SetPoint(5,2.75,0.829237259);
   graph->SetPoint(6,3.25,0.7842466126);
   graph->SetPoint(7,3.75,0.7188102845);
   graph->SetPoint(8,4.25,0.6860179485);
   graph->SetPoint(9,4.75,0.6808052881);
   
   TH1F *Graph_Graph1 = new TH1F("Graph_Graph1","Graph",100,0,5.2);
   Graph_Graph1->SetMinimum(0.6520445);
   Graph_Graph1->SetMaximum(0.9971736);
   Graph_Graph1->SetDirectory(0);
   Graph_Graph1->SetStats(0);
   Graph_Graph1->SetLineWidth(2);
   Graph_Graph1->SetMarkerStyle(20);
   Graph_Graph1->GetXaxis()->SetNdivisions(505);
   Graph_Graph1->GetXaxis()->SetLabelFont(42);
   Graph_Graph1->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph1->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph1->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph1->GetXaxis()->SetTitleFont(42);
   Graph_Graph1->GetYaxis()->SetLabelFont(42);
   Graph_Graph1->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph1->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph1->GetYaxis()->SetTitleFont(42);
   Graph_Graph1->GetZaxis()->SetLabelFont(42);
   Graph_Graph1->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph1->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph1->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph1);
   
   graph->Draw("l");
   
   graph = new TGraph(50);
   graph->SetName("Graph1");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);

   ci = TColor::GetColor("#00cc00");
   graph->SetLineColor(ci);
   graph->SetLineWidth(2);

   ci = TColor::GetColor("#00cc00");
   graph->SetMarkerColor(ci);
   graph->SetMarkerStyle(20);
   graph->SetPoint(0,0,1);
   graph->SetPoint(1,0.1,0.9937839832);
   graph->SetPoint(2,0.2,0.9875926679);
   graph->SetPoint(3,0.3,0.981412091);
   graph->SetPoint(4,0.4,0.9752283613);
   graph->SetPoint(5,0.5,0.969027536);
   graph->SetPoint(6,0.6,0.9627954879);
   graph->SetPoint(7,0.7,0.9565177811);
   graph->SetPoint(8,0.8,0.9501795467);
   graph->SetPoint(9,0.9,0.9437653555);
   graph->SetPoint(10,1,0.9372590981);
   graph->SetPoint(11,1.1,0.9306438535);
   graph->SetPoint(12,1.2,0.9239017691);
   graph->SetPoint(13,1.3,0.9170139277);
   graph->SetPoint(14,1.4,0.909960223);
   graph->SetPoint(15,1.5,0.9027192322);
   graph->SetPoint(16,1.6,0.8952680843);
   graph->SetPoint(17,1.7,0.8875823417);
   graph->SetPoint(18,1.8,0.8796358773);
   graph->SetPoint(19,1.9,0.8714007557);
   graph->SetPoint(20,2,0.8628471354);
   graph->SetPoint(21,2.1,0.853943182);
   graph->SetPoint(22,2.2,0.8446549904);
   graph->SetPoint(23,2.3,0.8349465463);
   graph->SetPoint(24,2.4,0.8247797177);
   graph->SetPoint(25,2.5,0.8141142814);
   graph->SetPoint(26,2.6,0.8029080214);
   graph->SetPoint(27,2.7,0.7911168694);
   graph->SetPoint(28,2.8,0.7787073973);
   graph->SetPoint(29,2.9,0.7670149426);
   graph->SetPoint(30,3,0.7544278597);
   graph->SetPoint(31,3.1,0.7408871574);
   graph->SetPoint(32,3.2,0.7263338557);
   graph->SetPoint(33,3.3,0.7107100688);
   graph->SetPoint(34,3.4,0.6939603417);
   graph->SetPoint(35,3.5,0.6760332418);
   graph->SetPoint(36,3.6,0.6568832446);
   graph->SetPoint(37,3.7,0.6364729037);
   graph->SetPoint(38,3.8,0.6147753049);
   graph->SetPoint(39,3.9,0.5917767707);
   graph->SetPoint(40,4,0.5674797645);
   graph->SetPoint(41,4.1,0.5419059052);
   graph->SetPoint(42,4.2,0.5150989867);
   graph->SetPoint(43,4.3,0.4871278073);
   graph->SetPoint(44,4.4,0.4580886906);
   graph->SetPoint(45,4.5,0.4281073859);
   graph->SetPoint(46,4.6,0.3973401938);
   graph->SetPoint(47,4.7,0.365974036);
   graph->SetPoint(48,4.8,0.3343634536);
   graph->SetPoint(49,4.9,0.302647175);
   
   TH1F *Graph_Graph2 = new TH1F("Graph_Graph2","Graph",100,0,5.39);
   Graph_Graph2->SetMinimum(0.2329119);
   Graph_Graph2->SetMaximum(1.069735);
   Graph_Graph2->SetDirectory(0);
   Graph_Graph2->SetStats(0);
   Graph_Graph2->SetLineWidth(2);
   Graph_Graph2->SetMarkerStyle(20);
   Graph_Graph2->GetXaxis()->SetNdivisions(505);
   Graph_Graph2->GetXaxis()->SetLabelFont(42);
   Graph_Graph2->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph2->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph2->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph2->GetXaxis()->SetTitleFont(42);
   Graph_Graph2->GetYaxis()->SetLabelFont(42);
   Graph_Graph2->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph2->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph2->GetYaxis()->SetTitleFont(42);
   Graph_Graph2->GetZaxis()->SetLabelFont(42);
   Graph_Graph2->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph2->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph2->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph2);
   
   graph->Draw("l");
   
   graph = new TGraph(50);
   graph->SetName("Graph2");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);
   graph->SetLineColor(4);
   graph->SetLineStyle(5);
   graph->SetLineWidth(2);

   ci = TColor::GetColor("#006600");
   graph->SetMarkerColor(ci);
   graph->SetMarkerStyle(20);
   graph->SetPoint(0,0,1);
   graph->SetPoint(1,0.1,0.9931273746);
   graph->SetPoint(2,0.2,0.9862063276);
   graph->SetPoint(3,0.3,0.9792227981);
   graph->SetPoint(4,0.4,0.972179661);
   graph->SetPoint(5,0.5,0.9650238827);
   graph->SetPoint(6,0.6,0.9577354153);
   graph->SetPoint(7,0.7,0.9504185158);
   graph->SetPoint(8,0.8,0.9430803966);
   graph->SetPoint(9,0.9,0.9357164458);
   graph->SetPoint(10,1,0.9284258587);
   graph->SetPoint(11,1.1,0.9209793347);
   graph->SetPoint(12,1.2,0.9132843825);
   graph->SetPoint(13,1.3,0.9053182673);
   graph->SetPoint(14,1.4,0.8970469502);
   graph->SetPoint(15,1.5,0.8883987239);
   graph->SetPoint(16,1.6,0.8793910571);
   graph->SetPoint(17,1.7,0.8699966169);
   graph->SetPoint(18,1.8,0.8601776424);
   graph->SetPoint(19,1.9,0.8499175641);
   graph->SetPoint(20,2,0.8390830614);
   graph->SetPoint(21,2.1,0.8275543915);
   graph->SetPoint(22,2.2,0.8152777712);
   graph->SetPoint(23,2.3,0.8022068883);
   graph->SetPoint(24,2.4,0.7884235354);
   graph->SetPoint(25,2.5,0.773742163);
   graph->SetPoint(26,2.6,0.7581104763);
   graph->SetPoint(27,2.7,0.7414905837);
   graph->SetPoint(28,2.8,0.7238635822);
   graph->SetPoint(29,2.9,0.7063457966);
   graph->SetPoint(30,3,0.687705256);
   graph->SetPoint(31,3.1,0.6679273373);
   graph->SetPoint(32,3.2,0.6470115599);
   graph->SetPoint(33,3.3,0.6254670945);
   graph->SetPoint(34,3.4,0.6027775163);
   graph->SetPoint(35,3.5,0.5787962001);
   graph->SetPoint(36,3.6,0.5536547605);
   graph->SetPoint(37,3.7,0.5276074031);
   graph->SetPoint(38,3.8,0.501061268);
   graph->SetPoint(39,3.9,0.4741600666);
   graph->SetPoint(40,4,0.447074954);
   graph->SetPoint(41,4.1,0.4199516333);
   graph->SetPoint(42,4.2,0.392897486);
   graph->SetPoint(43,4.3,0.3660200342);
   graph->SetPoint(44,4.4,0.3394883274);
   graph->SetPoint(45,4.5,0.3134895238);
   graph->SetPoint(46,4.6,0.2882151743);
   graph->SetPoint(47,4.7,0.2643912077);
   graph->SetPoint(48,4.8,0.2415657299);
   graph->SetPoint(49,4.9,0.2197100881);
   
   TH1F *Graph_Graph3 = new TH1F("Graph_Graph3","Graph",100,0,5.39);
   Graph_Graph3->SetMinimum(0.1416811);
   Graph_Graph3->SetMaximum(1.078029);
   Graph_Graph3->SetDirectory(0);
   Graph_Graph3->SetStats(0);
   Graph_Graph3->SetLineWidth(2);
   Graph_Graph3->SetMarkerStyle(20);
   Graph_Graph3->GetXaxis()->SetNdivisions(505);
   Graph_Graph3->GetXaxis()->SetLabelFont(42);
   Graph_Graph3->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph3->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph3->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph3->GetXaxis()->SetTitleFont(42);
   Graph_Graph3->GetYaxis()->SetLabelFont(42);
   Graph_Graph3->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph3->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph3->GetYaxis()->SetTitleFont(42);
   Graph_Graph3->GetZaxis()->SetLabelFont(42);
   Graph_Graph3->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph3->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph3->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph3);
   
   graph->Draw("l");
   
   gre = new TGraphErrors(3);
   gre->SetName("");
   gre->SetTitle("");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineWidth(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   gre->SetPoint(0,2.75,0.74);
   gre->SetPointError(0,0.25,0.08774964);
   gre->SetPoint(1,3.25,0.64);
   gre->SetPointError(1,0.25,0.04358899);
   gre->SetPoint(2,3.75,0.65);
   gre->SetPointError(2,0.25,0.0509902);
   
   TH1F *Graph_Graph2 = new TH1F("Graph_Graph2","",100,2.35,4.15);
   Graph_Graph2->SetMinimum(0.5732771);
   Graph_Graph2->SetMaximum(0.8508835);
   Graph_Graph2->SetDirectory(0);
   Graph_Graph2->SetStats(0);
   Graph_Graph2->SetLineWidth(2);
   Graph_Graph2->SetMarkerStyle(20);
   Graph_Graph2->GetXaxis()->SetNdivisions(505);
   Graph_Graph2->GetXaxis()->SetLabelFont(42);
   Graph_Graph2->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph2->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph2->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph2->GetXaxis()->SetTitleFont(42);
   Graph_Graph2->GetYaxis()->SetLabelFont(42);
   Graph_Graph2->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph2->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph2->GetYaxis()->SetTitleFont(42);
   Graph_Graph2->GetZaxis()->SetLabelFont(42);
   Graph_Graph2->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph2->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph2->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph2);
   
   gre->Draw("p");
   
   gre = new TGraphErrors(3);
   gre->SetName("");
   gre->SetTitle("");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineWidth(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   gre->SetPoint(0,2.75,0.74);
   gre->SetPointError(0,0.25,0.05);
   gre->SetPoint(1,3.25,0.64);
   gre->SetPointError(1,0.25,0.03);
   gre->SetPoint(2,3.75,0.65);
   gre->SetPointError(2,0.25,0.04);
   gre->Draw("z");
   
   gre = new TGraphErrors(1);
   gre->SetName("");
   gre->SetTitle("");
   gre->SetFillColor(1);
   gre->SetLineWidth(2);
   gre->SetMarkerStyle(20);
   gre->SetPoint(0,3.25,0.93);
   gre->SetPointError(0,0.75,0.26);
   
   TH1F *Graph_Graph_Graph13 = new TH1F("Graph_Graph_Graph13","",100,0,5);
   Graph_Graph_Graph13->SetMinimum(-0.7);
   Graph_Graph_Graph13->SetMaximum(2.5);
   Graph_Graph_Graph13->SetDirectory(0);
   Graph_Graph_Graph13->SetStats(0);
   Graph_Graph_Graph13->SetLineWidth(2);
   Graph_Graph_Graph13->SetMarkerStyle(20);
   Graph_Graph_Graph13->GetXaxis()->SetTitle("|#font[12]{y}|");
   Graph_Graph_Graph13->GetXaxis()->SetNdivisions(505);
   Graph_Graph_Graph13->GetXaxis()->SetLabelFont(132);
   Graph_Graph_Graph13->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph_Graph13->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph_Graph13->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph_Graph13->GetXaxis()->SetTitleFont(132);
   Graph_Graph_Graph13->GetYaxis()->SetTitle("#font[12]{R}_{FB}");
   Graph_Graph_Graph13->GetYaxis()->SetLabelFont(132);
   Graph_Graph_Graph13->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph_Graph13->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph_Graph13->GetYaxis()->SetTitleFont(132);
   Graph_Graph_Graph13->GetZaxis()->SetLabelFont(42);
   Graph_Graph_Graph13->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph_Graph13->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph_Graph13->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph_Graph13);
   
   gre->Draw("p");
   
   gre = new TGraphErrors(1);
   gre->SetName("");
   gre->SetTitle("");
   gre->SetFillColor(1);
   gre->SetLineWidth(2);
   gre->SetMarkerStyle(20);
   gre->SetPoint(0,3.25,0.93);
   gre->SetPointError(0,0.75,0.2720294);
   
   TH1F *Graph_Graph4 = new TH1F("Graph_Graph4","",100,0,5);
   Graph_Graph4->SetMinimum(-0.7);
   Graph_Graph4->SetMaximum(2.5);
   Graph_Graph4->SetDirectory(0);
   Graph_Graph4->SetStats(0);
   Graph_Graph4->SetLineWidth(2);
   Graph_Graph4->SetMarkerStyle(20);
   Graph_Graph4->GetXaxis()->SetTitle("|#font[12]{y}|");
   Graph_Graph4->GetXaxis()->SetNdivisions(505);
   Graph_Graph4->GetXaxis()->SetLabelFont(132);
   Graph_Graph4->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph4->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph4->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph4->GetXaxis()->SetTitleFont(132);
   Graph_Graph4->GetYaxis()->SetTitle("#font[12]{R}_{FB}");
   Graph_Graph4->GetYaxis()->SetLabelFont(132);
   Graph_Graph4->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph4->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph4->GetYaxis()->SetTitleFont(132);
   Graph_Graph4->GetZaxis()->SetLabelFont(42);
   Graph_Graph4->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph4->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph4->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph4);
   
   gre->Draw("z");
   myCB->Modified();
   myCB->cd();
   myCB->SetSelected(myCB);
}
