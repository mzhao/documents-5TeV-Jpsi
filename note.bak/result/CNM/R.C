{
//=========Macro generated from canvas: myCi/
//=========  (Mon Nov 30 19:58:34 2015) by ROOT version5.34/18
   TCanvas *myCi = new TCanvas("myCi", "",55,52,500,400);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   myCi->Range(-6.728395,-0.2658228,5.617284,1.632911);
   myCi->SetFillColor(0);
   myCi->SetBorderMode(0);
   myCi->SetBorderSize(2);
   myCi->SetTickx(1);
   myCi->SetTicky(1);
   myCi->SetLeftMargin(0.14);
   myCi->SetRightMargin(0.05);
   myCi->SetTopMargin(0.07);
   myCi->SetBottomMargin(0.14);
   myCi->SetFrameLineWidth(2);
   myCi->SetFrameBorderMode(0);
   myCi->SetFrameLineWidth(2);
   myCi->SetFrameBorderMode(0);
   
   TGraphErrors *gre = new TGraphErrors(2);
   gre->SetName("Graph0");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetLineWidth(3);
   gre->SetMarkerStyle(20);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,3.25,0.747042);
   gre->SetPointError(0,0.75,0.105136);
   gre->SetPoint(1,-3.25,0.549092);
   gre->SetPointError(1,0.75,0.15601);
   
   TH1F *Graph_Graph4 = new TH1F("Graph_Graph4","Graph",100,-5,5);
   Graph_Graph4->SetMinimum(0);
   Graph_Graph4->SetMaximum(1.5);
   Graph_Graph4->SetDirectory(0);
   Graph_Graph4->SetStats(0);
   Graph_Graph4->SetLineWidth(2);
   Graph_Graph4->SetMarkerStyle(20);
   Graph_Graph4->SetMarkerSize(1.5);
   Graph_Graph4->GetXaxis()->SetTitle("#font[12]{y}");
   Graph_Graph4->GetXaxis()->SetNdivisions(505);
   Graph_Graph4->GetXaxis()->SetLabelFont(132);
   Graph_Graph4->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph4->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph4->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph4->GetXaxis()->SetTitleFont(132);
   Graph_Graph4->GetYaxis()->SetTitle("#font[12]{R}");
   Graph_Graph4->GetYaxis()->SetLabelFont(132);
   Graph_Graph4->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph4->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph4->GetYaxis()->SetTitleFont(132);
   Graph_Graph4->GetZaxis()->SetLabelFont(42);
   Graph_Graph4->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph4->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph4->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph4);
   
   gre->Draw("ap");
   
   gre = new TGraphErrors(2);
   gre->SetName("Graph1");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineWidth(3);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(24);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,-3.71,0.54);
   gre->SetPointError(0,0.75,0.11);
   gre->SetPoint(1,2.78,0.66);
   gre->SetPointError(1,0.75,0.13);
   
   TH1F *Graph_Graph5 = new TH1F("Graph_Graph5","Graph",100,-5.259,4.329);
   Graph_Graph5->SetMinimum(0.394);
   Graph_Graph5->SetMaximum(0.826);
   Graph_Graph5->SetDirectory(0);
   Graph_Graph5->SetStats(0);
   Graph_Graph5->SetLineWidth(2);
   Graph_Graph5->SetMarkerStyle(20);
   Graph_Graph5->SetMarkerSize(1.5);
   Graph_Graph5->GetXaxis()->SetNdivisions(505);
   Graph_Graph5->GetXaxis()->SetLabelFont(42);
   Graph_Graph5->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph5->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph5->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph5->GetXaxis()->SetTitleFont(42);
   Graph_Graph5->GetYaxis()->SetLabelFont(42);
   Graph_Graph5->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph5->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph5->GetYaxis()->SetTitleFont(42);
   Graph_Graph5->GetZaxis()->SetLabelFont(42);
   Graph_Graph5->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph5->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph5->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph5);
   
   gre->Draw("p");
   
   gre = new TGraphErrors(1);
   gre->SetName("Graph2");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);

   Int_t ci;   // for color index setting
   ci = TColor::GetColor("#ff9900");
   gre->SetLineColor(ci);
   gre->SetLineWidth(3);

   ci = TColor::GetColor("#ff9900");
   gre->SetMarkerColor(ci);
   gre->SetMarkerStyle(22);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,0,0.68);
   gre->SetPointError(0,0.5,0.25);
   gre->Draw("z");
   
   gre = new TGraphErrors(1);
   gre->SetName("Graph3");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);

   ci = TColor::GetColor("#ff9900");
   gre->SetLineColor(ci);
   gre->SetLineWidth(3);

   ci = TColor::GetColor("#ff9900");
   gre->SetMarkerColor(ci);
   gre->SetMarkerStyle(22);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,0,0.68);
   gre->SetPointError(0,0.5,0.2);
   
   TH1F *Graph_Graph6 = new TH1F("Graph_Graph6","Graph",100,-0.6,0.6);
   Graph_Graph6->SetMinimum(0.44);
   Graph_Graph6->SetMaximum(0.92);
   Graph_Graph6->SetDirectory(0);
   Graph_Graph6->SetStats(0);
   Graph_Graph6->SetLineWidth(2);
   Graph_Graph6->SetMarkerStyle(20);
   Graph_Graph6->SetMarkerSize(1.5);
   Graph_Graph6->GetXaxis()->SetNdivisions(505);
   Graph_Graph6->GetXaxis()->SetLabelFont(42);
   Graph_Graph6->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph6->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph6->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph6->GetXaxis()->SetTitleFont(42);
   Graph_Graph6->GetYaxis()->SetLabelFont(42);
   Graph_Graph6->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph6->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph6->GetYaxis()->SetTitleFont(42);
   Graph_Graph6->GetZaxis()->SetLabelFont(42);
   Graph_Graph6->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph6->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph6->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph6);
   
   gre->Draw("p");
   
   gre = new TGraphErrors(2);
   gre->SetName("Graph4");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetLineWidth(3);
   gre->SetMarkerStyle(20);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,3.25,0.747042);
   gre->SetPointError(0,0.75,0.1494724);
   gre->SetPoint(1,-3.25,0.549092);
   gre->SetPointError(1,0.75,0.1730464);
   gre->Draw("z");
   
   gre = new TGraphErrors(2);
   gre->SetName("Graph0");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetLineWidth(3);
   gre->SetMarkerStyle(20);
   gre->SetMarkerSize(1.5);
   gre->SetPoint(0,3.25,0.747042);
   gre->SetPointError(0,0.75,0.105136);
   gre->SetPoint(1,-3.25,0.549092);
   gre->SetPointError(1,0.75,0.15601);
   
   TH1F *Graph_Graph_Graph47 = new TH1F("Graph_Graph_Graph47","Graph",100,-5,5);
   Graph_Graph_Graph47->SetMinimum(0);
   Graph_Graph_Graph47->SetMaximum(1.5);
   Graph_Graph_Graph47->SetDirectory(0);
   Graph_Graph_Graph47->SetStats(0);
   Graph_Graph_Graph47->SetLineWidth(2);
   Graph_Graph_Graph47->SetMarkerStyle(20);
   Graph_Graph_Graph47->SetMarkerSize(1.5);
   Graph_Graph_Graph47->GetXaxis()->SetTitle("#font[12]{y}");
   Graph_Graph_Graph47->GetXaxis()->SetNdivisions(505);
   Graph_Graph_Graph47->GetXaxis()->SetLabelFont(132);
   Graph_Graph_Graph47->GetXaxis()->SetLabelOffset(0.015);
   Graph_Graph_Graph47->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph_Graph47->GetXaxis()->SetTitleSize(0.06);
   Graph_Graph_Graph47->GetXaxis()->SetTitleFont(132);
   Graph_Graph_Graph47->GetYaxis()->SetTitle("#font[12]{R}");
   Graph_Graph_Graph47->GetYaxis()->SetLabelFont(132);
   Graph_Graph_Graph47->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph_Graph47->GetYaxis()->SetTitleSize(0.06);
   Graph_Graph_Graph47->GetYaxis()->SetTitleFont(132);
   Graph_Graph_Graph47->GetZaxis()->SetLabelFont(42);
   Graph_Graph_Graph47->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph_Graph47->GetZaxis()->SetTitleSize(0.06);
   Graph_Graph_Graph47->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph_Graph47);
   
   gre->Draw("p");
   
   TLegend *leg = new TLegend(0.45,0.7,0.92,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(132);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(3);
   leg->SetFillColor(10);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("Graph0","LHCb, #font[12]{#sqrt{s_{NN}}}= 5 TeV","ple");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(3);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(132);
   entry=leg->AddEntry("Graph1","ALICE, #font[12]{#sqrt{s_{NN}}}= 5 TeV","ple");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(3);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(24);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(132);
   entry=leg->AddEntry("Graph2","PHENIX, #font[12]{#sqrt{s_{NN}}}= 0.2 TeV","ple");

   ci = TColor::GetColor("#ff9900");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(3);

   ci = TColor::GetColor("#ff9900");
   entry->SetMarkerColor(ci);
   entry->SetMarkerStyle(22);
   entry->SetMarkerSize(1.5);
   entry->SetTextFont(132);
   leg->Draw();
   TLatex *   tex = new TLatex(0.18,0.78,"#splitline{LHCb}{#scale[1.0]{pPb #font[12]{#sqrt{s_{NN}}} = 5 TeV}}");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetLineWidth(2);
   tex->Draw();
   TLine *line = new TLine(-5,1,5,1);

   ci = TColor::GetColor("#cccccc");
   line->SetLineColor(ci);
   line->SetLineStyle(2);
   line->SetLineWidth(3);
   line->Draw();
      tex = new TLatex(0.18,0.28,"#font[12]{p}_{T} < 14 GeV/#font[12]{c}");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetTextSize(0.045);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.18,0.18,"Inclusive #font[12]{#psi}(2#font[12]{S})");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetTextSize(0.045);
   tex->SetLineWidth(2);
   tex->Draw();
   myCi->Modified();
   myCi->cd();
   myCi->SetSelected(myCi);
}
