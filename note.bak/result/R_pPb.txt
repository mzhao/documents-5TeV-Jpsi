% R
% forward
inclusive & $0.43 \pm 0.06 \pm 0.07$ \\
prompt   & $1.00 \pm 0.22 \pm 0.16$ \\
fromb  & $0.47 \pm 0.07 \pm 0.07$ \\
% backward
inclusive & $0.50 \pm 0.15 \pm 0.10$ \\
prompt    & $0.64 \pm 0.33 \pm 0.29$  \\
fromb     & $0.51 \pm 0.15 \pm 0.08$  \\

