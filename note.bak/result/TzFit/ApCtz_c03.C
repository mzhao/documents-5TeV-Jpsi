{
//=========Macro generated from canvas: tz_c0/
//=========  (Wed Sep 16 23:58:17 2015) by ROOT version5.34/18
   TCanvas *tz_c0 = new TCanvas("tz_c0", "",0,0,500,400);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   tz_c0->Range(3489.877,-42.31218,3847.901,259.9177);
   tz_c0->SetFillColor(0);
   tz_c0->SetBorderMode(0);
   tz_c0->SetBorderSize(2);
   tz_c0->SetTickx(1);
   tz_c0->SetTicky(1);
   tz_c0->SetLeftMargin(0.14);
   tz_c0->SetRightMargin(0.05);
   tz_c0->SetTopMargin(0.07);
   tz_c0->SetBottomMargin(0.14);
   tz_c0->SetFrameBorderMode(0);
   tz_c0->SetFrameBorderMode(0);
   
   TH1D *frame_54cf260__1 = new TH1D("frame_54cf260__1","",100,3540,3830);
   frame_54cf260__1->SetBinContent(1,238.7616);
   frame_54cf260__1->SetMaximum(238.7616);
   frame_54cf260__1->SetEntries(2);
   frame_54cf260__1->SetDirectory(0);
   frame_54cf260__1->SetStats(0);
   frame_54cf260__1->SetLineWidth(2);
   frame_54cf260__1->SetMarkerStyle(20);
   frame_54cf260__1->GetXaxis()->SetTitle("m_{#mu#mu} [MeV/c^{2}]");
   frame_54cf260__1->GetXaxis()->SetNdivisions(505);
   frame_54cf260__1->GetXaxis()->SetLabelFont(132);
   frame_54cf260__1->GetXaxis()->SetLabelOffset(0.015);
   frame_54cf260__1->GetXaxis()->SetLabelSize(0.05);
   frame_54cf260__1->GetXaxis()->SetTitleSize(0.06);
   frame_54cf260__1->GetXaxis()->SetTitleFont(132);
   frame_54cf260__1->GetYaxis()->SetTitle("Candidates / (20 MeV/c^{2})");
   frame_54cf260__1->GetYaxis()->SetLabelFont(132);
   frame_54cf260__1->GetYaxis()->SetLabelSize(0.05);
   frame_54cf260__1->GetYaxis()->SetTitleSize(0.06);
   frame_54cf260__1->GetYaxis()->SetTitleOffset(1.2);
   frame_54cf260__1->GetYaxis()->SetTitleFont(132);
   frame_54cf260__1->GetZaxis()->SetLabelFont(42);
   frame_54cf260__1->GetZaxis()->SetLabelSize(0.05);
   frame_54cf260__1->GetZaxis()->SetTitleSize(0.06);
   frame_54cf260__1->GetZaxis()->SetTitleFont(42);
   frame_54cf260__1->Draw("FUNC");
   
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(10);
   grae->SetName("mydata");
   grae->SetTitle("Histogram of sdata_plot__mass");
   grae->SetFillColor(1);
   grae->SetMarkerStyle(8);
   grae->SetPoint(0,3554.5,164);
   grae->SetPointError(0,14.5,14.5,12.31601,13.31601);
   grae->SetPoint(1,3583.5,150);
   grae->SetPointError(1,14.5,14.5,11.75765,12.75765);
   grae->SetPoint(2,3612.5,183);
   grae->SetPointError(2,14.5,14.5,13.03699,14.03699);
   grae->SetPoint(3,3641.5,157);
   grae->SetPointError(3,14.5,14.5,12.03994,13.03994);
   grae->SetPoint(4,3670.5,192);
   grae->SetPointError(4,14.5,14.5,13.36542,14.36542);
   grae->SetPoint(5,3699.5,198);
   grae->SetPointError(5,14.5,14.5,13.58013,14.58013);
   grae->SetPoint(6,3728.5,154);
   grae->SetPointError(6,14.5,14.5,11.91974,12.91974);
   grae->SetPoint(7,3757.5,135);
   grae->SetPointError(7,14.5,14.5,11.1297,12.1297);
   grae->SetPoint(8,3786.5,146);
   grae->SetPointError(8,14.5,14.5,11.59339,12.59339);
   grae->SetPoint(9,3815.5,154);
   grae->SetPointError(9,14.5,14.5,11.91974,12.91974);
   
   TH1F *Graph_mydata1 = new TH1F("Graph_mydata1","Histogram of sdata_plot__mass",100,3511,3859);
   Graph_mydata1->SetMinimum(114.9993);
   Graph_mydata1->SetMaximum(221.4511);
   Graph_mydata1->SetDirectory(0);
   Graph_mydata1->SetStats(0);
   Graph_mydata1->SetLineWidth(2);
   Graph_mydata1->SetMarkerStyle(20);
   Graph_mydata1->GetXaxis()->SetNdivisions(505);
   Graph_mydata1->GetXaxis()->SetLabelFont(42);
   Graph_mydata1->GetXaxis()->SetLabelOffset(0.015);
   Graph_mydata1->GetXaxis()->SetLabelSize(0.05);
   Graph_mydata1->GetXaxis()->SetTitleSize(0.06);
   Graph_mydata1->GetXaxis()->SetTitleFont(42);
   Graph_mydata1->GetYaxis()->SetLabelFont(42);
   Graph_mydata1->GetYaxis()->SetLabelSize(0.05);
   Graph_mydata1->GetYaxis()->SetTitleSize(0.06);
   Graph_mydata1->GetYaxis()->SetTitleFont(42);
   Graph_mydata1->GetZaxis()->SetLabelFont(42);
   Graph_mydata1->GetZaxis()->SetLabelSize(0.05);
   Graph_mydata1->GetZaxis()->SetTitleSize(0.06);
   Graph_mydata1->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_mydata1);
   
   grae->Draw("p");
   
   TGraph *graph = new TGraph(137);
   graph->SetName("sig_mass");
   graph->SetTitle("Projection of mass_pdf");
   graph->SetFillColor(1);

   Int_t ci;   // for color index setting
   ci = TColor::GetColor("#0000ff");
   graph->SetLineColor(ci);
   graph->SetLineStyle(2);
   graph->SetLineWidth(3);
   graph->SetMarkerStyle(20);
   graph->SetPoint(0,3537.1,0);
   graph->SetPoint(1,3537.1,0.383006285);
   graph->SetPoint(2,3540,0.383006285);
   graph->SetPoint(3,3542.9,0.3919137325);
   graph->SetPoint(4,3545.8,0.4012453601);
   graph->SetPoint(5,3548.7,0.4110322063);
   graph->SetPoint(6,3551.6,0.4213084141);
   graph->SetPoint(7,3554.5,0.432111628);
   graph->SetPoint(8,3557.4,0.4434834556);
   graph->SetPoint(9,3560.3,0.4554700026);
   graph->SetPoint(10,3563.2,0.4681224979);
   graph->SetPoint(11,3566.1,0.4814980253);
   graph->SetPoint(12,3569,0.4956603847);
   graph->SetPoint(13,3571.9,0.5106811097);
   graph->SetPoint(14,3574.8,0.5266406759);
   graph->SetPoint(15,3577.7,0.5436299436);
   graph->SetPoint(16,3580.6,0.5617518884);
   graph->SetPoint(17,3583.5,0.58112369);
   graph->SetPoint(18,3586.4,0.6018792691);
   graph->SetPoint(19,3589.3,0.6241723878);
   graph->SetPoint(20,3592.2,0.6481804651);
   graph->SetPoint(21,3595.1,0.674109309);
   graph->SetPoint(22,3598,0.7021990311);
   graph->SetPoint(23,3600.9,0.732731504);
   graph->SetPoint(24,3603.8,0.7660398541);
   graph->SetPoint(25,3606.7,0.8025206647);
   graph->SetPoint(26,3609.6,0.8426498423);
   graph->SetPoint(27,3612.5,0.8870034922);
   graph->SetPoint(28,3615.4,0.9362857545);
   graph->SetPoint(29,3618.3,0.9913664646);
   graph->SetPoint(30,3621.2,1.053332938);
   graph->SetPoint(31,3624.1,1.123562471);
   graph->SetPoint(32,3627,1.203825917);
   graph->SetPoint(33,3629.9,1.296439075);
   graph->SetPoint(34,3632.8,1.404489775);
   graph->SetPoint(35,3635.7,1.532188856);
   graph->SetPoint(36,3638.6,1.685431756);
   graph->SetPoint(37,3641.5,1.872734565);
   graph->SetPoint(38,3644.4,2.10687225);
   graph->SetPoint(39,3647.3,2.407921396);
   graph->SetPoint(40,3650.2,2.809346474);
   graph->SetPoint(41,3653.1,3.371391925);
   graph->SetPoint(42,3656,4.214570241);
   graph->SetPoint(43,3657.45,4.816921401);
   graph->SetPoint(44,3658.9,5.620161219);
   graph->SetPoint(45,3660.35,6.744898491);
   graph->SetPoint(46,3661.075,7.49485399);
   graph->SetPoint(47,3661.8,8.430596634);
   graph->SetPoint(48,3663.25,10.69051891);
   graph->SetPoint(49,3664.7,13.36925735);
   graph->SetPoint(50,3666.15,16.48860074);
   graph->SetPoint(51,3667.6,20.05526212);
   graph->SetPoint(52,3669.05,24.05696878);
   graph->SetPoint(53,3670.5,28.4591214);
   graph->SetPoint(54,3671.95,33.20244712);
   graph->SetPoint(55,3673.4,38.20205458);
   graph->SetPoint(56,3676.3,48.50919189);
   graph->SetPoint(57,3677.75,53.53585362);
   graph->SetPoint(58,3679.2,58.2684473);
   graph->SetPoint(59,3680.65,62.54465205);
   graph->SetPoint(60,3681.375,64.46241274);
   graph->SetPoint(61,3682.1,66.20868127);
   graph->SetPoint(62,3682.825,67.7665419);
   graph->SetPoint(63,3683.55,69.12063454);
   graph->SetPoint(64,3684.275,70.25740645);
   graph->SetPoint(65,3685,71.16533804);
   graph->SetPoint(66,3685.725,71.83513709);
   graph->SetPoint(67,3686.45,72.25989707);
   graph->SetPoint(68,3687.175,72.43521569);
   graph->SetPoint(69,3687.9,72.35927089);
   graph->SetPoint(70,3688.625,72.03285232);
   graph->SetPoint(71,3689.35,71.45934768);
   graph->SetPoint(72,3690.075,70.64468421);
   graph->SetPoint(73,3690.8,69.59722671);
   graph->SetPoint(74,3691.525,68.32763452);
   graph->SetPoint(75,3692.25,66.84868104);
   graph->SetPoint(76,3692.975,65.17503983);
   graph->SetPoint(77,3693.7,63.32304246);
   graph->SetPoint(78,3695.15,59.15598974);
   graph->SetPoint(79,3696.6,54.50090306);
   graph->SetPoint(80,3698.05,49.51955047);
   graph->SetPoint(81,3699.5,44.37288951);
   graph->SetPoint(82,3702.4,34.17462904);
   graph->SetPoint(83,3703.85,29.3730396);
   graph->SetPoint(84,3705.3,24.89785849);
   graph->SetPoint(85,3706.75,20.81340425);
   graph->SetPoint(86,3708.2,17.15901153);
   graph->SetPoint(87,3709.65,13.95113157);
   graph->SetPoint(88,3711.1,11.18651024);
   graph->SetPoint(89,3712.55,8.846018357);
   graph->SetPoint(90,3714,6.898728755);
   graph->SetPoint(91,3715.45,5.305891009);
   graph->SetPoint(92,3716.9,4.024534005);
   graph->SetPoint(93,3718.35,3.010515722);
   graph->SetPoint(94,3719.8,2.220926671);
   graph->SetPoint(95,3721.25,1.615829606);
   graph->SetPoint(96,3722.7,1.159377652);
   graph->SetPoint(97,3725.6,0.5725161157);
   graph->SetPoint(98,3728.5,0.2674376789);
   graph->SetPoint(99,3731.4,0.1181760743);
   graph->SetPoint(100,3734.3,0.04939791409);
   graph->SetPoint(101,3737.2,0.01953258615);
   graph->SetPoint(102,3740.1,0.007306055604);
   graph->SetPoint(103,3743,0.002585105576);
   graph->SetPoint(104,3745.9,0.0008652581173);
   graph->SetPoint(105,3748.8,0.0002739587442);
   graph->SetPoint(106,3751.7,8.205342957e-05);
   graph->SetPoint(107,3754.6,2.324772157e-05);
   graph->SetPoint(108,3757.5,6.230689942e-06);
   graph->SetPoint(109,3760.4,1.579661272e-06);
   graph->SetPoint(110,3763.3,3.788470314e-07);
   graph->SetPoint(111,3766.2,8.594802327e-08);
   graph->SetPoint(112,3769.1,1.844505623e-08);
   graph->SetPoint(113,3772,3.744520055e-09);
   graph->SetPoint(114,3774.9,7.190919468e-10);
   graph->SetPoint(115,3777.8,1.306305561e-10);
   graph->SetPoint(116,3780.7,2.244797722e-11);
   graph->SetPoint(117,3783.6,3.649066155e-12);
   graph->SetPoint(118,3786.5,5.611233579e-13);
   graph->SetPoint(119,3789.4,8.162196475e-14);
   graph->SetPoint(120,3792.3,1.123124312e-14);
   graph->SetPoint(121,3795.2,1.461910208e-15);
   graph->SetPoint(122,3798.1,1.800054506e-16);
   graph->SetPoint(123,3801,2.096634363e-17);
   graph->SetPoint(124,3803.9,2.310105582e-18);
   graph->SetPoint(125,3806.8,2.407759175e-19);
   graph->SetPoint(126,3809.7,2.373921479e-20);
   graph->SetPoint(127,3812.6,2.214072001e-21);
   graph->SetPoint(128,3815.5,1.953391154e-22);
   graph->SetPoint(129,3818.4,1.630267159e-23);
   graph->SetPoint(130,3821.3,1.287064851e-24);
   graph->SetPoint(131,3824.2,9.612008951e-26);
   graph->SetPoint(132,3827.1,6.790472421e-27);
   graph->SetPoint(133,3830,4.53793112e-28);
   graph->SetPoint(134,3830,4.53793112e-28);
   graph->SetPoint(135,3832.9,4.53793112e-28);
   graph->SetPoint(136,3832.9,0);
   
   TH1F *Graph_sig_mass1 = new TH1F("Graph_sig_mass1","Projection of mass_pdf",137,3507.52,3862.48);
   Graph_sig_mass1->SetMinimum(0);
   Graph_sig_mass1->SetMaximum(79.67874);
   Graph_sig_mass1->SetDirectory(0);
   Graph_sig_mass1->SetStats(0);
   Graph_sig_mass1->SetLineWidth(2);
   Graph_sig_mass1->SetMarkerStyle(20);
   Graph_sig_mass1->GetXaxis()->SetNdivisions(505);
   Graph_sig_mass1->GetXaxis()->SetLabelFont(42);
   Graph_sig_mass1->GetXaxis()->SetLabelOffset(0.015);
   Graph_sig_mass1->GetXaxis()->SetLabelSize(0.05);
   Graph_sig_mass1->GetXaxis()->SetTitleSize(0.06);
   Graph_sig_mass1->GetXaxis()->SetTitleFont(42);
   Graph_sig_mass1->GetYaxis()->SetLabelFont(42);
   Graph_sig_mass1->GetYaxis()->SetLabelSize(0.05);
   Graph_sig_mass1->GetYaxis()->SetTitleSize(0.06);
   Graph_sig_mass1->GetYaxis()->SetTitleFont(42);
   Graph_sig_mass1->GetZaxis()->SetLabelFont(42);
   Graph_sig_mass1->GetZaxis()->SetLabelSize(0.05);
   Graph_sig_mass1->GetZaxis()->SetTitleSize(0.06);
   Graph_sig_mass1->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_sig_mass1);
   
   graph->Draw("l");
   
   graph = new TGraph(106);
   graph->SetName("pol1_mass");
   graph->SetTitle("Projection of mass_pdf");
   graph->SetFillColor(1);

   ci = TColor::GetColor("#00cc00");
   graph->SetLineColor(ci);
   graph->SetLineStyle(2);
   graph->SetLineWidth(3);
   graph->SetMarkerStyle(20);
   graph->SetPoint(0,3537.1,0);
   graph->SetPoint(1,3537.1,165.6635146);
   graph->SetPoint(2,3540,165.6635146);
   graph->SetPoint(3,3542.9,165.4455617);
   graph->SetPoint(4,3545.8,165.2278956);
   graph->SetPoint(5,3548.7,165.0105159);
   graph->SetPoint(6,3551.6,164.7934222);
   graph->SetPoint(7,3554.5,164.576614);
   graph->SetPoint(8,3557.4,164.3600912);
   graph->SetPoint(9,3560.3,164.1438531);
   graph->SetPoint(10,3563.2,163.9278996);
   graph->SetPoint(11,3566.1,163.7122302);
   graph->SetPoint(12,3569,163.4968445);
   graph->SetPoint(13,3571.9,163.2817423);
   graph->SetPoint(14,3574.8,163.066923);
   graph->SetPoint(15,3577.7,162.8523863);
   graph->SetPoint(16,3580.6,162.6381319);
   graph->SetPoint(17,3583.5,162.4241593);
   graph->SetPoint(18,3586.4,162.2104683);
   graph->SetPoint(19,3589.3,161.9970584);
   graph->SetPoint(20,3592.2,161.7839293);
   graph->SetPoint(21,3595.1,161.5710805);
   graph->SetPoint(22,3598,161.3585119);
   graph->SetPoint(23,3600.9,161.1462228);
   graph->SetPoint(24,3603.8,160.9342131);
   graph->SetPoint(25,3606.7,160.7224823);
   graph->SetPoint(26,3609.6,160.51103);
   graph->SetPoint(27,3612.5,160.299856);
   graph->SetPoint(28,3615.4,160.0889598);
   graph->SetPoint(29,3618.3,159.878341);
   graph->SetPoint(30,3621.2,159.6679994);
   graph->SetPoint(31,3624.1,159.4579344);
   graph->SetPoint(32,3627,159.2481459);
   graph->SetPoint(33,3629.9,159.0386333);
   graph->SetPoint(34,3632.8,158.8293964);
   graph->SetPoint(35,3635.7,158.6204348);
   graph->SetPoint(36,3638.6,158.411748);
   graph->SetPoint(37,3641.5,158.2033359);
   graph->SetPoint(38,3644.4,157.9951979);
   graph->SetPoint(39,3647.3,157.7873338);
   graph->SetPoint(40,3650.2,157.5797431);
   graph->SetPoint(41,3653.1,157.3724256);
   graph->SetPoint(42,3656,157.1653808);
   graph->SetPoint(43,3658.9,156.9586084);
   graph->SetPoint(44,3661.8,156.752108);
   graph->SetPoint(45,3664.7,156.5458794);
   graph->SetPoint(46,3667.6,156.339922);
   graph->SetPoint(47,3670.5,156.1342356);
   graph->SetPoint(48,3673.4,155.9288198);
   graph->SetPoint(49,3676.3,155.7236743);
   graph->SetPoint(50,3679.2,155.5187987);
   graph->SetPoint(51,3682.1,155.3141926);
   graph->SetPoint(52,3685,155.1098557);
   graph->SetPoint(53,3687.9,154.9057876);
   graph->SetPoint(54,3690.8,154.701988);
   graph->SetPoint(55,3693.7,154.4984565);
   graph->SetPoint(56,3696.6,154.2951928);
   graph->SetPoint(57,3699.5,154.0921966);
   graph->SetPoint(58,3702.4,153.8894674);
   graph->SetPoint(59,3705.3,153.6870049);
   graph->SetPoint(60,3708.2,153.4848088);
   graph->SetPoint(61,3711.1,153.2828787);
   graph->SetPoint(62,3714,153.0812142);
   graph->SetPoint(63,3716.9,152.8798151);
   graph->SetPoint(64,3719.8,152.678681);
   graph->SetPoint(65,3722.7,152.4778114);
   graph->SetPoint(66,3725.6,152.2772062);
   graph->SetPoint(67,3728.5,152.0768648);
   graph->SetPoint(68,3731.4,151.8767871);
   graph->SetPoint(69,3734.3,151.6769725);
   graph->SetPoint(70,3737.2,151.4774209);
   graph->SetPoint(71,3740.1,151.2781318);
   graph->SetPoint(72,3743,151.0791049);
   graph->SetPoint(73,3745.9,150.8803398);
   graph->SetPoint(74,3748.8,150.6818362);
   graph->SetPoint(75,3751.7,150.4835938);
   graph->SetPoint(76,3754.6,150.2856122);
   graph->SetPoint(77,3757.5,150.0878911);
   graph->SetPoint(78,3760.4,149.8904301);
   graph->SetPoint(79,3763.3,149.6932289);
   graph->SetPoint(80,3766.2,149.4962871);
   graph->SetPoint(81,3769.1,149.2996045);
   graph->SetPoint(82,3772,149.1031806);
   graph->SetPoint(83,3774.9,148.9070151);
   graph->SetPoint(84,3777.8,148.7111077);
   graph->SetPoint(85,3780.7,148.515458);
   graph->SetPoint(86,3783.6,148.3200658);
   graph->SetPoint(87,3786.5,148.1249306);
   graph->SetPoint(88,3789.4,147.9300521);
   graph->SetPoint(89,3792.3,147.7354301);
   graph->SetPoint(90,3795.2,147.5410641);
   graph->SetPoint(91,3798.1,147.3469538);
   graph->SetPoint(92,3801,147.1530988);
   graph->SetPoint(93,3803.9,146.959499);
   graph->SetPoint(94,3806.8,146.7661538);
   graph->SetPoint(95,3809.7,146.573063);
   graph->SetPoint(96,3812.6,146.3802262);
   graph->SetPoint(97,3815.5,146.1876432);
   graph->SetPoint(98,3818.4,145.9953135);
   graph->SetPoint(99,3821.3,145.8032368);
   graph->SetPoint(100,3824.2,145.6114129);
   graph->SetPoint(101,3827.1,145.4198413);
   graph->SetPoint(102,3830,145.2285218);
   graph->SetPoint(103,3830,145.2285218);
   graph->SetPoint(104,3832.9,145.2285218);
   graph->SetPoint(105,3832.9,0);
   
   TH1F *Graph_pol1_mass2 = new TH1F("Graph_pol1_mass2","Projection of mass_pdf",106,3507.52,3862.48);
   Graph_pol1_mass2->SetMinimum(0);
   Graph_pol1_mass2->SetMaximum(182.2299);
   Graph_pol1_mass2->SetDirectory(0);
   Graph_pol1_mass2->SetStats(0);
   Graph_pol1_mass2->SetLineWidth(2);
   Graph_pol1_mass2->SetMarkerStyle(20);
   Graph_pol1_mass2->GetXaxis()->SetNdivisions(505);
   Graph_pol1_mass2->GetXaxis()->SetLabelFont(42);
   Graph_pol1_mass2->GetXaxis()->SetLabelOffset(0.015);
   Graph_pol1_mass2->GetXaxis()->SetLabelSize(0.05);
   Graph_pol1_mass2->GetXaxis()->SetTitleSize(0.06);
   Graph_pol1_mass2->GetXaxis()->SetTitleFont(42);
   Graph_pol1_mass2->GetYaxis()->SetLabelFont(42);
   Graph_pol1_mass2->GetYaxis()->SetLabelSize(0.05);
   Graph_pol1_mass2->GetYaxis()->SetTitleSize(0.06);
   Graph_pol1_mass2->GetYaxis()->SetTitleFont(42);
   Graph_pol1_mass2->GetZaxis()->SetLabelFont(42);
   Graph_pol1_mass2->GetZaxis()->SetLabelSize(0.05);
   Graph_pol1_mass2->GetZaxis()->SetTitleSize(0.06);
   Graph_pol1_mass2->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_pol1_mass2);
   
   graph->Draw("l");
   
   graph = new TGraph(136);
   graph->SetName("masspdf");
   graph->SetTitle("Projection of mass_pdf");
   graph->SetFillColor(1);

   ci = TColor::GetColor("#ff0000");
   graph->SetLineColor(ci);
   graph->SetLineWidth(3);
   graph->SetMarkerStyle(20);
   graph->SetPoint(0,3537.1,0);
   graph->SetPoint(1,3537.1,166.0465208);
   graph->SetPoint(2,3540,166.0465208);
   graph->SetPoint(3,3542.9,165.8374754);
   graph->SetPoint(4,3545.8,165.629141);
   graph->SetPoint(5,3548.7,165.4215481);
   graph->SetPoint(6,3551.6,165.2147306);
   graph->SetPoint(7,3554.5,165.0087257);
   graph->SetPoint(8,3557.4,164.8035746);
   graph->SetPoint(9,3560.3,164.5993231);
   graph->SetPoint(10,3563.2,164.3960221);
   graph->SetPoint(11,3566.1,164.1937282);
   graph->SetPoint(12,3569,163.9925049);
   graph->SetPoint(13,3571.9,163.7924234);
   graph->SetPoint(14,3574.8,163.5935636);
   graph->SetPoint(15,3577.7,163.3960162);
   graph->SetPoint(16,3580.6,163.1998837);
   graph->SetPoint(17,3583.5,163.005283);
   graph->SetPoint(18,3586.4,162.8123476);
   graph->SetPoint(19,3589.3,162.6212308);
   graph->SetPoint(20,3592.2,162.4321097);
   graph->SetPoint(21,3595.1,162.2451899);
   graph->SetPoint(22,3598,162.0607109);
   graph->SetPoint(23,3600.9,161.8789543);
   graph->SetPoint(24,3603.8,161.700253);
   graph->SetPoint(25,3606.7,161.525003);
   graph->SetPoint(26,3609.6,161.3536799);
   graph->SetPoint(27,3612.5,161.1868595);
   graph->SetPoint(28,3615.4,161.0252455);
   graph->SetPoint(29,3618.3,160.8697075);
   graph->SetPoint(30,3621.2,160.7213323);
   graph->SetPoint(31,3624.1,160.5814969);
   graph->SetPoint(32,3627,160.4519718);
   graph->SetPoint(33,3629.9,160.3350724);
   graph->SetPoint(34,3632.8,160.2338862);
   graph->SetPoint(35,3635.7,160.1526236);
   graph->SetPoint(36,3638.6,160.0971798);
   graph->SetPoint(37,3641.5,160.0760705);
   graph->SetPoint(38,3644.4,160.1020702);
   graph->SetPoint(39,3647.3,160.1952552);
   graph->SetPoint(40,3650.2,160.3890896);
   graph->SetPoint(41,3653.1,160.7438175);
   graph->SetPoint(42,3656,161.379951);
   graph->SetPoint(43,3657.45,161.878882);
   graph->SetPoint(44,3658.9,162.5787696);
   graph->SetPoint(45,3660.35,163.6002227);
   graph->SetPoint(46,3661.075,164.2985616);
   graph->SetPoint(47,3661.8,165.1827047);
   graph->SetPoint(48,3663.25,167.3394787);
   graph->SetPoint(49,3664.7,169.9151367);
   graph->SetPoint(50,3666.15,172.9314675);
   graph->SetPoint(51,3667.6,176.3951841);
   graph->SetPoint(52,3669.05,180.2940137);
   graph->SetPoint(53,3670.5,184.593357);
   graph->SetPoint(54,3671.95,189.233941);
   graph->SetPoint(55,3673.4,194.1308744);
   graph->SetPoint(56,3676.3,204.2328662);
   graph->SetPoint(57,3677.75,209.1570564);
   graph->SetPoint(58,3679.2,213.787246);
   graph->SetPoint(59,3680.65,217.961114);
   graph->SetPoint(60,3681.375,219.8277316);
   graph->SetPoint(61,3682.1,221.5228738);
   graph->SetPoint(62,3682.825,223.029625);
   graph->SetPoint(63,3683.55,224.332625);
   graph->SetPoint(64,3684.275,225.4183211);
   graph->SetPoint(65,3685,226.2751937);
   graph->SetPoint(66,3685.725,226.8939506);
   graph->SetPoint(67,3686.45,227.2676851);
   graph->SetPoint(68,3687.175,227.3919951);
   graph->SetPoint(69,3687.9,227.2650585);
   graph->SetPoint(70,3688.625,226.8876649);
   graph->SetPoint(71,3689.35,226.2632019);
   graph->SetPoint(72,3690.075,225.397597);
   graph->SetPoint(73,3690.8,224.2992147);
   graph->SetPoint(74,3691.525,222.9787145);
   graph->SetPoint(75,3692.25,221.4488698);
   graph->SetPoint(76,3692.975,219.7243541);
   graph->SetPoint(77,3693.7,217.821499);
   graph->SetPoint(78,3695.15,213.552781);
   graph->SetPoint(79,3696.6,208.7960959);
   graph->SetPoint(80,3698.05,203.7132118);
   graph->SetPoint(81,3699.5,198.4650861);
   graph->SetPoint(82,3702.4,188.0640964);
   graph->SetPoint(83,3703.85,183.1612424);
   graph->SetPoint(84,3705.3,178.5848634);
   graph->SetPoint(85,3706.75,174.3992778);
   graph->SetPoint(86,3708.2,170.6438203);
   graph->SetPoint(87,3709.65,167.3349421);
   graph->SetPoint(88,3711.1,164.4693889);
   graph->SetPoint(89,3712.55,162.0280316);
   graph->SetPoint(90,3714,159.979943);
   graph->SetPoint(91,3715.45,158.2863725);
   graph->SetPoint(92,3716.9,156.9043491);
   graph->SetPoint(93,3718.35,155.7897307);
   graph->SetPoint(94,3719.8,154.8996076);
   graph->SetPoint(95,3722.7,153.6371891);
   graph->SetPoint(96,3725.6,152.8497223);
   graph->SetPoint(97,3728.5,152.3443025);
   graph->SetPoint(98,3731.4,151.9949631);
   graph->SetPoint(99,3734.3,151.7263705);
   graph->SetPoint(100,3737.2,151.4969535);
   graph->SetPoint(101,3740.1,151.2854378);
   graph->SetPoint(102,3743,151.08169);
   graph->SetPoint(103,3745.9,150.881205);
   graph->SetPoint(104,3748.8,150.6821102);
   graph->SetPoint(105,3751.7,150.4836759);
   graph->SetPoint(106,3754.6,150.2856355);
   graph->SetPoint(107,3757.5,150.0878973);
   graph->SetPoint(108,3760.4,149.8904317);
   graph->SetPoint(109,3763.3,149.6932293);
   graph->SetPoint(110,3766.2,149.4962872);
   graph->SetPoint(111,3769.1,149.2996045);
   graph->SetPoint(112,3772,149.1031806);
   graph->SetPoint(113,3774.9,148.9070151);
   graph->SetPoint(114,3777.8,148.7111077);
   graph->SetPoint(115,3780.7,148.515458);
   graph->SetPoint(116,3783.6,148.3200658);
   graph->SetPoint(117,3786.5,148.1249306);
   graph->SetPoint(118,3789.4,147.9300521);
   graph->SetPoint(119,3792.3,147.7354301);
   graph->SetPoint(120,3795.2,147.5410641);
   graph->SetPoint(121,3798.1,147.3469538);
   graph->SetPoint(122,3801,147.1530988);
   graph->SetPoint(123,3803.9,146.959499);
   graph->SetPoint(124,3806.8,146.7661538);
   graph->SetPoint(125,3809.7,146.573063);
   graph->SetPoint(126,3812.6,146.3802262);
   graph->SetPoint(127,3815.5,146.1876432);
   graph->SetPoint(128,3818.4,145.9953135);
   graph->SetPoint(129,3821.3,145.8032368);
   graph->SetPoint(130,3824.2,145.6114129);
   graph->SetPoint(131,3827.1,145.4198413);
   graph->SetPoint(132,3830,145.2285218);
   graph->SetPoint(133,3830,145.2285218);
   graph->SetPoint(134,3832.9,145.2285218);
   graph->SetPoint(135,3832.9,0);
   
   TH1F *Graph_masspdf3 = new TH1F("Graph_masspdf3","Projection of mass_pdf",136,3507.52,3862.48);
   Graph_masspdf3->SetMinimum(0);
   Graph_masspdf3->SetMaximum(250.1312);
   Graph_masspdf3->SetDirectory(0);
   Graph_masspdf3->SetStats(0);
   Graph_masspdf3->SetLineWidth(2);
   Graph_masspdf3->SetMarkerStyle(20);
   Graph_masspdf3->GetXaxis()->SetNdivisions(505);
   Graph_masspdf3->GetXaxis()->SetLabelFont(42);
   Graph_masspdf3->GetXaxis()->SetLabelOffset(0.015);
   Graph_masspdf3->GetXaxis()->SetLabelSize(0.05);
   Graph_masspdf3->GetXaxis()->SetTitleSize(0.06);
   Graph_masspdf3->GetXaxis()->SetTitleFont(42);
   Graph_masspdf3->GetYaxis()->SetLabelFont(42);
   Graph_masspdf3->GetYaxis()->SetLabelSize(0.05);
   Graph_masspdf3->GetYaxis()->SetTitleSize(0.06);
   Graph_masspdf3->GetYaxis()->SetTitleFont(42);
   Graph_masspdf3->GetZaxis()->SetLabelFont(42);
   Graph_masspdf3->GetZaxis()->SetLabelSize(0.05);
   Graph_masspdf3->GetZaxis()->SetTitleSize(0.06);
   Graph_masspdf3->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_masspdf3);
   
   graph->Draw("l");
   
   TH1D *frame_54cf260__2 = new TH1D("frame_54cf260__2","",100,3540,3830);
   frame_54cf260__2->SetBinContent(1,238.7616);
   frame_54cf260__2->SetMaximum(238.7616);
   frame_54cf260__2->SetEntries(2);
   frame_54cf260__2->SetDirectory(0);
   frame_54cf260__2->SetStats(0);
   frame_54cf260__2->SetLineWidth(2);
   frame_54cf260__2->SetMarkerStyle(20);
   frame_54cf260__2->GetXaxis()->SetTitle("m_{#mu#mu} [MeV/c^{2}]");
   frame_54cf260__2->GetXaxis()->SetNdivisions(505);
   frame_54cf260__2->GetXaxis()->SetLabelFont(132);
   frame_54cf260__2->GetXaxis()->SetLabelOffset(0.015);
   frame_54cf260__2->GetXaxis()->SetLabelSize(0.05);
   frame_54cf260__2->GetXaxis()->SetTitleSize(0.06);
   frame_54cf260__2->GetXaxis()->SetTitleFont(132);
   frame_54cf260__2->GetYaxis()->SetTitle("Candidates / (20 MeV/c^{2})");
   frame_54cf260__2->GetYaxis()->SetLabelFont(132);
   frame_54cf260__2->GetYaxis()->SetLabelSize(0.05);
   frame_54cf260__2->GetYaxis()->SetTitleSize(0.06);
   frame_54cf260__2->GetYaxis()->SetTitleOffset(1.2);
   frame_54cf260__2->GetYaxis()->SetTitleFont(132);
   frame_54cf260__2->GetZaxis()->SetLabelFont(42);
   frame_54cf260__2->GetZaxis()->SetLabelSize(0.05);
   frame_54cf260__2->GetZaxis()->SetTitleSize(0.06);
   frame_54cf260__2->GetZaxis()->SetTitleFont(42);
   frame_54cf260__2->Draw("AXISSAME");
   TLatex *   tex = new TLatex(0.12,0.25,"#splitline{LHCb preliminary}{#scale[1.0]{pPb(Fwd) #sqrt{s_{NN}} = 5 TeV}}");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetTextSize(0.045);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.64,0.3,"#font[122]{-}4.0< y <#font[122]{-}2.5");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetTextSize(0.045);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.64,0.25,"p_{T} < 14 GeV/c");
tex->SetNDC();
   tex->SetTextFont(132);
   tex->SetTextSize(0.045);
   tex->SetLineWidth(2);
   tex->Draw();
   tz_c0->Modified();
   tz_c0->cd();
   tz_c0->SetSelected(tz_c0);
}
