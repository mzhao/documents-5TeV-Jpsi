   \midrule
 $0.0-2.0$    & $\xx0.9 \pm \xx0.6 \pm \xx0.2$ \\
 $2.0-3.0$    & $\xx1.1 \pm \xx0.7 \pm \xx0.1$ \\
 $3.0-5.0$    & $\xx1.0 \pm \xx0.5 \pm \xx0.3$ \\
 $5.0-7.0$    & $\xx1.3 \pm \xx0.7 \pm \xx0.4$ \\
 $7.0- 14$    & $\xx1.2 \pm \xx0.7 \pm \xx0.1$ \\
 \bottomrule


