% This file was converted from HTML to LaTeX with
% gnuhtml2latex program
% (c) Tomasz Wegrzanowski <maniek@beer.com> 1999
% (c) Gunnar Wolf <gwolf@gwolf.org> 2005-2010
% Version : 0.4.

% $Id: appendix.tex 32339 2013-03-08 16:15:05Z tgershon $
% ===============================================================================
% Purpose: appendix to the standard template: standard symbol alises from Ulrik
% Author: Tomasz Skwarnicki
% Created on: 2009-09-24
% ===============================================================================
%\clearpage
\section{Luminosity calibration}
\label{sec:Appendix:lumi}
%To show the details of the $3\%$ luminosity uncertainty provided 
%in Table~\ref{tab:JpsiSystematics} of Section~\ref{sec:systematics}, 
%we provide the luminosity calibration results in this appendix.
The luminosity is determined with an uncertainty of 1.9\% (2.1\%) for the
forward (backward) sample.
The absolute luminosity is calibrated by measuring a visible (``effective'')
cross-section using the van der Meer method as described in~\cite{vanderMeer:1968zz,Burkhardt:2007zzc}.
The number of visible interactions (using the same definition) is counted
during physics data taking and the luminosity is determined using the measured
cross-section.
The details of previous absolute luminosity measurements at \lhcb that were
performed in 2010 for $pp$ collisions are described in
\cite{LHCb-PAPER-2011-015}.

The beam intensity normalisation was the dominating source of systematic
uncertainty in 2010, accounting for 2.7\% of the total 3.6\% uncertainty.
The \lhc beam intensity instrumentation was studied and calibrated~\cite{Barschel:1425904},
%[http://cds.cern.ch/record/1425904], 
reducing the corresponding systematic
uncertainty in $pA$ calibrations to a negligible level of less than 0.4\%.
The nominal beam positions during van der Meer scans are calculated from the
\lhc magnet currents.
The true length scale of the beam movements is calibrated using vertices
measured with the VELO.
The \lhc optics setup was almost identical in the two proton-lead beam
configurations, thus the length scale calibrations are expected to be identical.
However, a difference of about 1\% is observed and it is assigned as a
systematic uncertainty to both calibrations.
The difference of the visible cross-section measurement in repeated van der
Meer scans is the dominating source of uncertainty in the luminosity of the
backward sample, accounting for 1.3\%.
While the scan difference in the calibration of the forward sample is small,
a 0.6\% uncertainty is assigned based on an estimate of the uncontrolled beam
drifts and the scanning conditions.

Other potential sources of systematic errors are considered in addition to the
ones in the 2010 calibration.
Beam-beam effects, which are modifying the bunch shape and position during van
der Meer scans have a negligible impact on $pA$ calibrations due the low bunch
intensities of (1.3-1.7)$ \times 10^{10}$ elementary charges.
The movements of the luminous region in $z$ can impact the definition of
the effective process used for calibration due to non-uniform VELO acceptance in $z$.
This effect has been studied and corrected for, and a systematic uncertainty of
less than 0.4\% is assigned.
The factorizability of the $xy$ bunch distribution is checked by using a simple
non-factorizable model.
The found discrepancy of less than 0.2\% is assigned as a systematic
uncertainty.
During $pA$ data taking the average number of visible interactions per crossing
was very low (0.01-0.02), effectively increasing the fraction of beam-gas and
beam-beam induced backgrounds.
While the former is well understood and subtracted, the latter is not.
The effect is estimated with the difference in visible event rates obtained
with and without using a fiducial volume cut in the effective process
definition.
Corresponding systematic uncertainties are conservatively assigned to both
absolute calibration and visible interaction counting, since the two are not
equally affected.
The magnitude of the uncertainties ranges from 0.6\% to 1\% and they are among
the dominating uncertainties.

The luminosity calibration results are provided in this appendix.
The summary of the luminosity calibration uncertainties is shown in
Table~\ref{tab:LumiSummary}, and the details of the luminosity calibration are
shown in Table~\ref{tab:LumiCalibration}.
We appreciate Rosen Matev for providing these tables.

%The summary table shows that the luminosity uncertainty for forward collisions 
%is $1.86\%$, and that for backward collisions is $2.13\%$.
%In both case, the uncorrelated part is dominant.


% summary of lumi calibration
\begin{table}[!htbp]
\caption{ \small 
  Summary of the luminosity calibration uncertainties in \pPb collisions in 2013.
}
\begin{center}
\begin{tabular}{lcccccc}
\toprule
Calib. & $\sigma$ & total uncert. & abs.calib & rel.calib & correlated & uncorrelated \\
\midrule
Foward & $2.093\pm0.039$ b & 1.86\% & 1.48\% & 1.12\% & 0.67\% & 1.72\% \\
Backward & $2.084\pm0.044$ b & 2.13\% & 1.96\% & 0.84\% & 0.88\% & 1.93\% \\
\bottomrule
\end{tabular}\end{center}
\label{tab:LumiSummary}
\end{table}

In Table~\ref{tab:LumiCalibration}, 
to avoid double counting, the uncertainties in brackets do not contribute to the final uncertainty. They are included other uncertainties and are mentioned here for completeness.

% Luminosity calibration results table
\begin{sidewaystable}[!htbp]  
\caption{ \small %captions should be a little bit smaller than main text
    Luminosity calibration results.
} 
\begin{small}
 %\begin{center} 
\scalebox{1.0}{%
 \begin{tabular}{@{}llccccc@{}}   
 \toprule
 Group & source & cor(Fwd,Bwd) & corr\_Fwd & uncertainty\_Fwd & corr\_Bwd & uncertainty\_Bwd \\ 
 Intensity & DCCT   & 1          &          & 0.31\%          &          & 0.34\% \\
 Intensity & FBCT offset & 0     &          & 0.10\%          &          & 0.07\% \\
 Intensity & BPTX cross-check & 0 &         & 0.01\%          &          & 0.07\% \\
 Intensity & Ghost charge &      & +0.80\%  & 0.14\%          & +0.97\%  & 0.19\% \\
 Intensity & Satellite correction & 0 & +0.19\% & 0.07\%      & +0.20\%  & 0.10\% \\
 Intensity & Satellite correction (missing) & 0 & & 0.00\%    &          & 0.00\% \\
 \midrule
 Rate (mu) & Bkgd stat. fluctuation & 0 & & 0.34\%    &          & 0.21\% \\
 Rate (mu) & Bkgd subtraction & 1 & & 0.56\%    &          & 0.81\% \\
 Rate (mu) & Mu efficiency(z) & 1 & +0.09\% & 0.04\%    & +0.11\%        & 0.06\% \\
 \midrule
 Separation & LHC length scale & 0 & -2.14\% & 1.07\%    & -1.04\%        & 1.07\% \\
 Separation & Drift            & 0 &         & (0.59\%)  &                & (1.03\%) \\
 Separation & VELO scale       & 1 &         & 0.05\%    &                & 0.05\% \\
 \midrule
 Beam-beam effects & Beam-beam correction & 1 &     & 0.03\%    &                & 0.08\% \\
 Beam-beam effects & Dynamic beta         & 1 & -0.03\% & (0.00\%)  & -0.07\%    & (0.00\%) \\
 Beam-beam effects & Beam-beam deflection & 1 & +0.15\% & (0.00\%)  & +0.16\%    & (0.00\%) \\
 \midrule
 Shape & Simplest NF model    & 1 &     & 0.16\%    &                & 0.05\% \\
 Shape & Fit bias (MC)        &   &     & 0.20\%    &                & 0.20\% \\
 Shape & Free bkgd params     & 0 &     & 0.08\%    &                & 0.02\% \\
 \midrule
 Reproducibility & Scan variation       & 0 &     & (0.12\%)  &                & (1.31\%) \\
 Reproducibility & Scan var. \& drift   & 0 &     & 0.59\%    &                & 1.31\% \\
 \midrule
 Statistical & Statistical          & 0 &     & 0.24\%    &                & 0.24\% \\
 \midrule
 Relative & Bkgd subtraction     & 0 &     & 0.95\%    &                & 0.73\% \\
 Relative & Mu efficiency(z)     & 0 & -0.07\% & 0.42\% & -0.11\%       & 0.24\% \\
 Relative & Bunch spread+stat. bias (MC) & 0 & +0.03\% & 0.01\% & +0.03\% & 0.02\% \\
 Relative & Ratio stability      & 0 &     & 0.42\%    &                & 0.35\% \\
 \bottomrule
  \end{tabular}%
  %}\end{center}
 }\end{small}
\label{tab:LumiCalibration}
\end{sidewaystable}

%
%\textbf{group}
%\textbf{source}
%\textbf{cor(pA,Ap)}
%\textbf{correction\_pA}
%\textbf{uncertainty\_pA}
%\textbf{correction\_Ap}
%\textbf{uncertainty\_Ap}
%\textbf{comment}
%
%Intensity
%DCCT
%1
%
%0.31\%
%
%0.34\%
%The 68\% envelope value
%
%
%Intensity
%FBCT offset
%0
%
%0.10\%
%
%0.07\%
%FBCT zero offset vs. common offset
%
%
%Intensity
%BPTX cross-check
%0
%
%0.01\%
%
%0.07\%
%sigma(BPTX,common\_offset) vs. sigma(baseline)
%
%
%Intensity
%Ghost charge
%0
%+0.80\%
%0.14\%
%+0.97\%
%0.19\%
%
%
%
%Intensity
%Satellite correction
%0
%+0.19\%
%0.07\%
%+0.20\%
%0.10\%
%Official uncertainty plus beginning/end difference
%
%
%Intensity
%Satellite correction (missing)
%0
%
%0.00\%
%
%0.00\%
%Missing dafa for one beam: 100\% of other beam
%
%
%Rate (mu)
%Background stat. fluctuation
%0
%
%0.34\%
%
%0.21\%
%Propagated background uncertainty
%
%
%Rate (mu)
%Background subtraction
%1
%
%0.56\%
%
%0.81\%
%sigma(RZVelo) vs. sigma(baseline)
%
%
%Rate (mu)
%Mu efficiency(z)
%1
%+0.09\%
%0.04\%
%+0.11\%
%0.06\%
%50\% of correction
%
%
%Separation
%LHC length scale
%0
%-2.14\%
%1.07\%
%-1.04\%
%1.07\%
%Full difference Jan13 - Feb13
%
%
%Separation
%Drift
%0
%
%(0.59\%)
%
%(1.03\%)
%Drift uncertainty
%
%
%Separation
%VELO scale
%1
%
%0.05\%
%
%0.05\%
%Upper limit on transverse VELO scale
%
%
%Beam-beam effects
%Beam-beam corrections
%1
%
%0.03\%
%
%0.08\%
%Maximum of bcid uncertainties
%
%
%Beam-beam effects
%Dynamic beta
%1
%-0.03\%
%(0.00\%)
%-0.07\%
%(0.00\%)
%Dynamic beta correction
%
%
%Beam-beam effects
%Beam-beam deflection
%1
%+0.15\%
%(0.00\%)
%+0.16\%
%(0.00\%)
%Beam-beam deflection correction
%
%
%Shape
%Simplest NF model
%1
%
%0.16\%
%
%0.05\%
%Simplest non-factorizable model vs. baseline
%
%
%Shape
%Fit bias (MC)
%
%
%0.20\%
%
%0.20\%
%From MC
%
%
%Shape
%Free background params
%0
%
%0.08\%
%
%0.02\%
%Free background parameters
%
%
%Reproducibility
%Scan variation
%0
%
%(0.12\%)
%
%(1.31\%)
%Maximum pair deviation from average
%
%
%Reproducibility
%Scan var. \& drift
%0
%
%0.59\%
%
%1.31\%
%max(drift uncertainty, max pair deviation)
%
%
%Statistical
%Statistical
%0
%
%0.24\%
%
%0.24\%
%Fudged with sqrt(chi2/df)
%
%
%Relative
%Background subtraction
%0
%
%0.95\%
%
%0.73\%
%|Nint(RZVelo)/Nint(Velo) - 1|
%
%
%Relative
%Mu efficiency(z)
%0
%-0.07\%
%0.42\%
%-0.11\%
%0.24\%
%Difference of Velo/L0Calo ratio at z=0 and max(|z|)
%
%
%Relative
%Bunch spread + stat. bias (MC)
%0
%+0.03\%
%0.01\%
%+0.03\%
%0.02\%
%Bias of log0 method due to statistics and averaging (spread)
%
%
%Relative
%Ratio stability
%0
%
%0.42\%
%
%0.35\%
%Maximum of full variation w.r.t L0Calo and Vertex
%
%
%
%
%
%
%
%\textbf{calibration}
%\textbf{sigma}
%\textbf{total uncertainty}
%\textbf{abs. calib}
%\textbf{rel. calib}
%\textbf{correlated}
%\textbf{uncorrelated}
%
%
%
%
%pA
%(2.093 +- 0.039) b
%1.86\%
%1.48\%
%1.12\%
%0.67\%
%1.72\%
%
%
%Ap
%(2.084 +- 0.044) b
%2.13\%
%1.96\%
%0.84\%
%0.88\%
%1.93\%
%


