%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Efficiencies}
\label{sec:psi2SEfficiencies}
The total efficiency includes the geometrical acceptance $\epsAcc$,
the reconstruction efficiency $\epsRec$, the ProbNNmu cut efficiency $\epsPID$,
and the trigger efficiency $\epsTrig$,
\begin{equation}
\label{eq:psi2STotalEff}
 \eps_\mathrm{tot} = 
 \epsAcc\cdot\epsRec\cdot\epsPID\cdot\epsTrig.
\end{equation}
The efficiencies \epsPID and \epsTrig are obtained directly from data, 
as described in Sections~\ref{sec:MuonIDEfficiency} and~\ref{sec:psi2STriggerEfficiency}.
The geometrical acceptance and the reconstruction efficiency are estimated
using the simulation samples mentioned in Section~\ref{sec:datasets}, 
which is reweighted to reproduce the data samples in \pPb collisions.

Figure~\ref{fig:psi2SMCvsData} shows the background-subtracted distributions,
using the \sPlot\ technique, of \pt, $p$, the rapidity in the laboratory frame \ylab,
and the track multiplicity for the simulated $pp$ sample 
and the data samples in $\pPb$ collisions.
It shows that the kinematic distributions agree with each other 
within the statistical uncertainties.
The distribution of the track multiplicity in the $\pPb$ forward sample 
is close to that in the simulated $pp$ sample,
while the $\pPb$ backward sample indicates higher track multiplicity.
The comparisons for some other variables and comparisons of \pt,$p$,y using weighted are shown in Appendix~\ref{sec:Appendix:DataMCComparison}. 
%% MC Data comparison for pA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[!htb]
\begin{center}
\includegraphics[width=0.49 \textwidth]{comparison/pt.pdf}
\includegraphics[width=0.49 \textwidth]{comparison/p.pdf}
\includegraphics[width=0.49 \textwidth]{comparison/y.pdf}
\includegraphics[width=0.49 \textwidth]{comparison/nTracks.pdf}
\vspace*{-0.5cm}
\end{center}
\caption{ \small 
    Comparisons of the $\psitwos$ transverse momentum, momentum,
    the rapidity in the laboratory frame, 
    and the number of best tracks between the \pPb forward/backward data
    and the 2012 $8\protect\tev$ $pp$ MC sample.
    All the selection criteria are applied for the data samples, 
    and the signal distributions are extracted using the \sPlot\ technique.
}
\label{fig:psi2SMCvsData}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

As one knows, the track multiplicity does not change the angular distribution
of the dimuon final states for a $\psitwos$ meson with given $(\pt,y)$.
Therefore, the geometrical acceptance is independent on the track multiplicity. 
The tracking efficiency, which is included in the reconstruction efficiency,
should be corrected by using the tracking efficiency ratio table
provided by the tracking group~\cite{LHCb-DP-2013-002}. 
To use this table, the MC sample is first weighted to data according to track multiplicity. 
It is found that the effect of high multiplicity is negligible 
in the \pPb forward sample, and small (about $1\%$) in \pPb backward samples.
It is also found that the effect of the high multiplicity on the efficiency of 
muon identification (IsMuon) is also small (around $1\%$).

For the efficiencies estimated from the simulation sample, 
a fine 2-dimensional binning scheme as a function of $\pt$ and $y$ is adopted,
and the efficiency correction is performed event-by-event.
The effect due to the finite bin size is studied and quoted as systematic uncertainty.
%The efficiencies for this analysis are compared to previous measurements as a cross-check, as shown in Appendix~\ref{sec:Appendix:EfficiencyComparison}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Geometrical acceptance}
\label{sec:jpsiAcceptance}
The generator-level geometrical acceptance is defined as
\begin{equation}
\label{eq:psi2SAcc}
\epsAcc=\frac{\psitwos \mbox{ in bin (\pt,\y) with both muons in \lhcb}}
{\psitwos \mbox{ generated in bin (\pt,\y)}}
\end{equation}
To estimate the geometrical acceptance, a simulation sample 
of $\psitwos$ from $pp$ collisions at the generator level is generated using \gauss v42r4,
as mentioned in Section~\ref{sec:datasets}.
The $\psitwos$ mesons are generated with no polarisation.
The centre-of-mass energy of $pp$ collisions in the simulation is $5\tev$,
and all particles are generated in the centre-of-mass frame of the $pp$ system.
The efficiency in each \pt and $\ylab$ bin, $\epsAcc(\pt,\ylab)$,
is then calculated in the $pp$ lab frame.
A simple boost in rapidity will give the efficiency for $\pPb$ forward or backward collisions, \ie,
$\epsAcc^{\pPb}(\pt,y)=\epsAcc^{pp}(\pt,\ylab=y+y_0)$,
where $y_0=+0.465$ for forward and $-0.465$ for backward collisions.
The two-dimensional efficiency table is used for acceptance corrections.
Figure~\ref{fig:psi2SAccEff5TeV} shows the acceptance 
for \psitwos mesons in the forward (left) and backward (right) samples
in the nucleon-nucleon centre-of-mass frame.
The values are given in Appendix~\ref{sec:Appendix:TablesOfAcc}.
It has been checked that the acceptance obtained from a direct shift
of the efficiency table in rapidity is identical to what is calculated
from the kinematic variables of $\psitwos$ mesons and its daughters in the laboratory frame.
%% eff_acc
\begin{figure}[!htb]
\begin{center}
\includegraphics[width=0.49 \textwidth]{result/Acc/pA_geneff_1D.pdf}
\includegraphics[width=0.49 \textwidth]{result/Acc/Ap_geneff_1D.pdf}
\vspace*{-0.5cm}
\end{center}
\caption{ 
       Acceptance of \psitwos mesons as a function of \pt in $y$ bins
      for the $\pPb$ (left) forward and (right) backward samples
      in the nucleon-nucleon centre-of-mass frame. 
      It is assumed that the \psitwos is unpolarised in the centre-of-mass frame.
}
\label{fig:psi2SAccEff5TeV}
\end{figure}


%
%%%%%%%%%% Reconstruction efficiency %%%%%%%%%%%
\subsection{Reconstruction efficiency}
The reconstruction efficiency, which covers the $\psitwos$ detection, reconstruction
and selection efficiencies, is defined as:
\begin{equation}
\epsRec=
  \frac{\mbox{$\psitwos$ detected, reconstructed and selected in bin (\pt,\y)}}
       {\mbox{$\psitwos$ in bin (\pt,\y) with both $\mu$ in \lhcb }}
\end{equation}
Here, the mass window in the fit is also applied as a selection cut in the numerator.
The mass window cut will lose a fraction of around $3\%$ $\psitwos$ candidates 
that have lower invariant mass due to the radiation tail.
Therefore, this effect is taken into account in the reconstruction efficiency determination.
The denominator here is the same as the numerator in Eq.~\ref{eq:psi2SAcc}.
Strictly speaking, this equation is not a pure efficiency if the resolution of $\pt$ 
or $y$ is comparable to the bin size and hence the migration of events between bins 
is not negligible. 
However, the resolutions of $\pt$ and $y$ are only a few percent of the bin sizes. 
Therefore, the migration of events between bins is negligible.

The reconstruction efficiency is calculated from the 2012 $8\tev$ $pp$ MC sample 
and corrected by using the tracking efficiency ratio table 
from the tracking group~\cite{lhcbtrackweb}.
The latest tracking efficiency ratio table is used in this analysis,
which was released on April 11, 2013, corresponding to Stripping 20 and Reco14.

The following steps are taken individually for the $\pPb$ forward 
and backward samples to get the corrected reconstruction efficiency.
Firstly, the MC sample is weighted according to the background subtracted 
track multiplicity observed in data.
Then, the tracking efficiency ratio table, determined by the tracking group, is
applied to extract the tracking efficiency correction factors.
Finally, the correction factors are applied to obtain the tracking efficiency in data.
The reconstruction efficiencies as functions of \pt and $y$ are shown in
Appendix~\ref{sec:Appendix:TablesOfRec}.
Figure~\ref{fig:psi2SRecEffPt} shows the reconstruction efficiency 
as functions of $\pt$ and $y$ for \psitwos mesons in the $\pPb$ (left) forward and (right) backward samples.
As observed in $pp$ collisions~\cite{LHCb-PAPER-2011-003,LHCb-PAPER-2013-016,LHCb-PAPER-2013-052}, 
the reconstruction efficiencies in the two rapidity bins close to the edges 
of fiducial region are much lower than those in other bins. 
Another common feature is that there is a drop of efficiency around $\pt(\psitwos)=3.5\gevc$, which is a kinematic effect caused by the $\pt$ cut on the two muons. 

%%%%%% Reconstruction efficiency as functions of pt in pA and Ap
\begin{figure}[!htb]
\begin{center}
\includegraphics[width=0.49 \textwidth]{result/Rec/pAD_psip_receff_2D.pdf}
\includegraphics[width=0.49 \textwidth]{result/Rec/ApD_psip_receff_2D.pdf}
\vspace*{-0.5cm}
\end{center}
\caption{ 
    Reconstruction efficiencies of \psitwos as functions of \pt in $y$ bins
    in the $\pPb$ (left) forward and (right) backward samples.
}
\label{fig:psi2SRecEffPt}
\end{figure}

%%%%%%%%% Reconstruction efficiency %%%%%%%%%%%
\subsection{Efficiency of ProbNNmu cut}
\label{sec:MuonIDEfficiency}
The efficiency of the ProbNNmu cut for each muon track is obtained from 
\pPb forward and backward samples separately 
using the tag-and-probe method with the $\jpsi\to\mumu$ sample, and is estimated in each $(p,\pt)$ bin.
In the tag-and-probe method \jpsi candidates are reconstructed by applying
the ProbNNmu cut on one muon (``tag") as tight as possible ($>0.55$ in this analysis),
while leaving the other (``probe") without a ProbNNmu cut.
The other selection criteria are summarized in Table~\ref{tab:jpsi}.
Finally, about 26600 (12000) \jpsi events are selected with all the forward (backward) 
sample when $\mup$ is tagged.
The number of \jpsi events with $\mun$ tag is almost the same.
\begin{table}[t] 
  \caption{ 
   Selection criteria for \jpsi candidates.
The ProbNNmu cut is not included in the table.
    }
\begin{center}
\begin{tabular}{lc}
\toprule
  Variable            & Value \\
\midrule
  Track $\chisqndf$   & $<3$  \\
  Track $\pt$         & $>700\mevc$ \\
  Muon Identification & IsMuon \\
  Vertex $p(\chisqndf)$ & $>0.5\%$ \\
  Clone distance        & $>5000$ \\
  Mass cut            & $2900<m<3210\mevcc$ \\
\bottomrule 
  \end{tabular}\end{center}
\label{tab:jpsi}
\end{table}
It is observed that the efficiencies for $\mup$ and $\mun$ tracks are the same
within uncertainties. Therefore, the average of the efficiencies for $\mup$ and $\mun$
is used to reduce the statistical uncertainty.
A two-dimensional table of the ProbNNmu cut efficiency for muon track,
given in $p$ and \pt bins, is used to calculate the ProbNNmu cut efficiency
of each $\psitwos$ candidate,
\begin{equation}
\label{eq:psi2SPIDmu}
\epsPID(\psitwos)
=\eps_{\mu}(p^{\mup},\pt^{\mup})\cdot\eps_{\mu}(p^{\mun},\pt^{\mun}),
\end{equation}
where $\epsPID(\psitwos)$ is the ProbNNmu cut efficiency for the given 
$\psitwos$ candidate, while $\eps_{\mu}(p^{\mu^\pm},\pt^{\mu^\pm})$ is the ProbNNmu cut 
efficiency for the $\mu^\pm$ track of the $\psitwos$ candidate.
The ProbNNmu cut efficiencies for the muon track are almost $100\%$ for high \pt and/or $p$ tracks,
as shown in Figure~\ref{fig:PIDmuEff} and Appendix~\ref{sec:Appendix:TablesOfPID}.
%%%%% PIDmu cut efficiency table 
\begin{figure}[!htb]
\begin{center}
\includegraphics[width=0.49 \textwidth]{result/Prob/pA_ProbNNmu_1D.pdf}
\includegraphics[width=0.49 \textwidth]{result/Prob/Ap_ProbNNmu_1D.pdf}
\vspace*{-0.5cm}
\end{center}
\caption{ 
The ProbNNmu cut efficiency table for the muon track as functions of $p$ and \pt
in the \pPb (left) forward and (right) backward samples.
}
\label{fig:PIDmuEff}
\end{figure}
%

%%%%%%%%% Trigger efficiency %%%%%%%%%%
\subsection{Trigger efficiency}
\label{sec:psi2STriggerEfficiency}
As mentioned in Section~\ref{sec:Detector}, during the period of the 2013 $\pPb$ run 
the \lone trigger was simply an interaction trigger, which rejected empty events.
For events with $\psitwos$ candidates, the \lone efficiency is $100\%$.
The \hlttwo is in pass through mode, which receive all events filtered by \hltone.
Therefore, only \hltone trigger efficiency needs to be considered,
defined as:
\begin{equation}
\label{eq:psi2STrigEff}
\epsTrig=\frac{\mbox{$\psitwos$ selected and TOS of \hltone in bin (\pt,\y)}}
                       {\mbox{$\psitwos$ selected in bin (\pt,\y)}}
\end{equation}

The trigger efficiency is estimated from data using the TISTOS method,
which is defined as
\begin{equation}
\label{eq:psi2STrigEffTISTOS}
\epsTrig^{\mathrm{TISTOS}}
=\frac{N_\mathrm{TIS\&TOS}}{N_\mathrm{TIS}}
\end{equation}
where $N_\mathrm{TISTOS}$ means the number of events 
which pass both TIS (trigger independent of signal)
and TOS (trigger on signal),
and $N_\mathrm{TIS}$ means the number of events which pass TIS requirement.
The error is calculated as
\begin{equation}
\label{eq:TISTOSerror}
(\Delta\eps)^2=\left(\eps\frac{\Delta{}N_\mathrm{TIS}}{N_\mathrm{TIS}}\right)^2
+(1-2\eps)\left(\frac{\Delta{}N_\mathrm{TIS\&TOS}}{N_\mathrm{TIS}}\right)^2
\end{equation}
The TIS line used is Hlt1MBMicroBiasVelo, while the TOS line used is Hlt1SingleMuonNoIP.

It was shown in the study of the $\jpsi$ production 
in \pPb collisions~\cite{LHCb-PAPER-2013-052}
that the dependence of the trigger efficiency 
on \pt and $y$ is not significant.
This means that the trigger can be determined without dividing the samples into
$\pt$ and $y$ bins.
It also indicated that the trigger efficiency is almost independent on the track multiplicity.
The trigger efficiency is obtained to be $\epsTrig=(97.2\pm2.0)\%$ in the \pPb forward sample,
and $\epsTrig=(96.9\pm7.5)\%$ in backward sample.
The uncertainty of trigger efficiency in the backward sample is large due to the limited sample size.
According to the study of \jpsi cross-section in \pPb data,
the trigger efficiencies in forward and backward region are almost the same.
%With $Prob_{\mu}>0.10$, the trigger efficiency obtained in the forward sample is estimated to be $(92.2\pm3.4)\%$.
Therefore, in this analysis, the weighted average of both, $\epsTrig=(97.2\pm1.9)\%$ is applied to both forward and backward samples.
The uncertainty of this efficiency is taken as the systematic uncertainty due to the trigger efficiency.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
